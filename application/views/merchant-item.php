<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <script src="https://kit.fontawesome.com/8609eee3c1.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/merchant-item.css">
    <?php $this->load->view('sidebar-navbar');?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row align-item-center mt-4">
                <div class="col-lg-4">
                    <h3>Merchant Items</h3>
                </div>
                <form class="tittle-box-form1 col-lg-8" role="search">
                    <div class="row justify-content-center">
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">
                        </div>
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">
                        </div>
                        <div class="col-7 col-lg-3">
                            <input class="form-control input-edit" type="search" placeholder="Status" aria-label="Search">
                        </div>
                    </div>
                </form>
            </div>
        </div>   
        <div class="container"> 
            <div class="card">
                <div class="row p-4">
                    <form class="tittle-box-form2 col-lg-10" role="search">
                        <div class="row">
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="Search" aria-label="Search">                                             
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                    <option selected>Status</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>                                            
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2">
                            <a href="<?php echo base_url();?>add_product" class="btn add-btn">
                                Add
                            </a>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-borderless text-left">
                        <thead>
                            <th>Merchant</th>
                            <th>Branch</th>
                            <th>Category</th>
                            <th class="text-nowrap">Sub Category</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th class="text-nowrap">Created Date</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>
                        </thead>
                        <tbody>
                        <?php
                            foreach ($productlist as $list) { 
                                ?>
                            <tr>
                                <td class="text-nowrap"><?php echo $list->product_name?></td>
                                <td><?php echo $list->product_branch?></td>
                                <td><?php echo $list->product_category?></td>
                                <td><?php echo $list->product_sub_category?></td>
                                <td><img class="me-2 product-image" src="<?php echo base_url();?>uploads/<?php echo $list->product_image?>"><?php echo $list->product_nam?></td>
                                <td><?php echo $list->product_price?></td>
                                <td><?php echo $list->date?></td>
                                <td class="text-center <?php echo ($list->product_status == 'active')?"tbl-active":""?>"><?php echo $list->product_status?></td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <button>...</button>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Merchantitem/view_items?product_id=<?php echo $list->product_id?>"><img class="pe-2" src="<?php echo base_url();?>assets/images/eye.png">View</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Merchantitem/edit_product?product_id=<?php echo $list->product_id?>"><img class="pe-2" src="<?php echo base_url();?>assets/images/ic_baseline-edit.png">Edit</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Merchantitem/delete_product?<?php echo $list->product_id?>"><img class="pe-2" src="<?php echo base_url();?>assets/images/ant-design_delete-outlined.png">delete</a></li>  
                                        </ul>
                                    </div> 
                                </td>
                            </tr>
                        <?php }  
                        ?> 
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-end mx-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>                
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
</body>