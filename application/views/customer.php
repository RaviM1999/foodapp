<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/customer.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Customer<h3>
                </div>
            </div>
            <div class="card">
                <div class="row p-4">
                    <form class="tittle-box-form2 col-lg-10">
                        <div class="row">
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="Search" aria-label="Search">                                             
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                    <option disabled selected>Status</option>
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                   
                                </select>                                            
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2">
                        <a type="button" class="btn add-btn" href="#" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">
                            + Add
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table table-borderless text-left">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th class="w-25">Customer Name</th>
                                        <th>Phone Number</th>
                                        <th>Email Id</th>
                                        <th>Created date</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; foreach($customer_data as $customer) { ?>
                                        <tr>
                                            <td><?php echo $i?></td>
                                            <td><img class="product-image" src="<?php echo base_url();?>uploads/<?php echo $customer->customer_image?>"> <?php echo $customer->customer_firstname?> <?php echo $customer->customer_lastname?></td>
                                            <td><?php echo $customer->customer_no?></td>
                                            <td><?php echo $customer->customer_email?></td>
                                            <td><?php echo $customer->date?></td>
                                            <td class="text-center <?php echo($customer->customer_status == 'active')?"tbl-active":""?>"><?php echo $customer->customer_status?></td>
                                            <td class="text-center">
                                                <div class="dropdown">
                                                    <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        <button>...</i></button>
                                                    </a>
                                                    <ul class="dropdown-menu collapse">
                                                        <li><a class="dropdown-item" data-bs-target="#modalToggle<?php echo $customer->customer_id?>" data-bs-toggle="modal" href="#"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                                        <li><a class="dropdown-item" href="<?php echo base_url();?>Customer/customer_delete?<?php echo $customer->customer_id?>"><i class="material-icons text-center">&#xe254;</i>delete</a></li>
                                                        <li><a class="dropdown-item" href="<?php echo base_url()?>Customer/customer_edit?customer_id=<?php echo $customer->customer_id?>"><i class="material-icons text-center">&#xe872;</i>Edit</a></li>
                                                    </ul>
                                                </div>
                                            </td>                                           
                                        </tr>
                                    <?php $i++; } ?>   
                                </tbody>
                            </table>
                            <?php foreach($customer_data as $customer) { ?>
                                <div class="customise-modal">
                                    <div class="modal fade" id="modalToggle<?php echo $customer->customer_id?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body p-4">
                                                    <div class="total-view">
                                                        <h3 class="text-center">View Customer Details</h3>
                                                        <div class="row justify-content-end">
                                                            <div class="col-3">
                                                                <a href="<?php echo base_url()?>Customer/customer_edit?customer_id=<?php echo $customer->customer_id?>">
                                                                    <button class="btn add-btn">Edit</button>
                                                                </a>  
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <img src="<?php echo base_url();?>uploads/<?php echo $customer->customer_image?>" class="img-fluid">
                                                            </div>
                                                            <div class="col-lg-8 mb-5s">
                                                                <div class="row">
                                                                    <h3 class="col-12"><?php echo $customer->customer_firstname?> <?php echo $customer->customer_lastname?></h3>
                                                                    <div class="col">
                                                                        <table class="table table-borderless customer-table">
                                                                            <tr>
                                                                                <td>Phone Number</td>
                                                                                <td><?php echo $customer->customer_no?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Email</td>
                                                                                <td><?php echo $customer->customer_email?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Created Date</td>
                                                                                <td><?php echo $customer->date?></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Status</td>
                                                                                <td class="<?php echo($customer->customer_status == 'active')?"tbl-active":""?>"><?php echo $customer->customer_status?></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>                                    
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php $i++; } ?> 
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-body p-4">
                                <div id="page-1">
                                    <h3 class="text-center">Add Customer</h3>
                                    <form action="<?php echo base_url();?>Customer/customer" class="row" id="customer-form" enctype="multipart/form-data" method="post">
                                        <div class="col-lg-4">
                                            <label class="form-label d-none" for="firstname">First Name</label>
                                            <input type="text" class="form-control input-edit-form" name="customer_firstname" id="firstname"
                                                placeholder="First Name">
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-label d-none" for="lastname">Last Name</label>
                                            <input type="text" class="form-control input-edit-form" name="customer_lastname" id="lastname"
                                                placeholder="Last Name">
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-label d-none" for="contactno">Contact No</label>
                                            <input type="text" class="form-control input-edit-form numbersOnly" name="customer_no" id="Contact No"
                                                placeholder="Contact No">
                                            </select>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-label d-none" for="email">Email</label>
                                            <input type="text" class="form-control input-edit-form" name="customer_email" id="email"
                                                placeholder="Email">
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-label d-none" for="status">Status</label>
                                            <select class="form-select input-edit-form" name="customer_status" id="status" aria-label="Default select example">
                                                <option disabled selected>Status</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-5">
                                            <label for="image1" class="form-label d-none">Select Your Image</label>
                                            <input class="form-control input-edit-form" name="customer_image" id="image1" type="file">
                                        </div>
                                        <div class="row justify-content-center ms-2 my-5">
                                            <div class="col-3">
                                                <button type="submit" class="btn add-btn mb-2" id="submit">Submit</button>
                                                <button type="button" class="btn cancel-btn mb-2">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="page-2 d-none" id="page-2">
                                    <h3 class="text-center">View Customer Details</h3>
                                    <div class="row justify-content-end">
                                        <div class="col-3">
                                            <button class="btn add-btn">Edit</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <img src="<?php echo base_url();?>assets/images/customerfull.png" class="img-fluid">
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <h3 class="col-12">John Rick</h3>
                                                <div class="col">
                                                    <table class="table table-borderless customer-table">
                                                        <tr>
                                                            <td>Phone Number</td>
                                                            <td>Mr.Spencer Wise</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email</td>
                                                            <td>sri@gmail.com</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Created Date</td>
                                                            <td>22/03/2023</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Status</td>
                                                            <td class="tbl-active">Active</td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>                                    
                                    </div>
                                    <div class="row justify-content-center ms-5">
                                        <div class="col-3">
                                            <button type="button" class="btn cancel-btn" id="cancel">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end mx-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#customer-form").validate({
                rules: {
                    customer_firstname: {
                        required: true,
                    },
                    customer_lastname: {
                        required: true,
                    },
                    customer_no: {
                        required: true,
                    },
                    customer_email: {
                        required: true,
                    },
                    customer_status: {
                        required: true,
                    },
                    customer_image: {
                        required: true,
                    }                    
                },
                messages: {
                    customer_firstname: {
                        required: "Enter Your First Name",
                    },
                    customer_lastname: {
                        required: "Enter Your Last Name",
                    },
                    customer_contactno: {
                        required: "enter Contact No",
                    },
                    customer_email: {
                        required: "Enter Email",
                    },
                    customer_status: {
                        required: "Select Status",
                    },
                    customer_image: {
                        required: "Select Image",
                    }
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });
            // let page1 =document.getElementById("page-1");
            // let page2 =document.getElementById("page-2");          
            

            // let submit =document.getElementById("submit");
            // submit.addEventListener("click",hide1);

            // function hide1() {
            //     if ($("#customer-form").valid() == true) {
            //         page1.classList.add("d-none");                    
            //         page2.classList.remove("d-none");
            //     }
            // }
            // let cancel =document.getElementById("cancel");
            // cancel.addEventListener("click", show1);

            // function show1() {
            //     page1.classList.remove("d-none");                    
            //     page2.classList.add("d-none");
            // }
        });

</script>
</body>
