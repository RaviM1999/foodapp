<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Base</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
</head>
<body>
    <div class="first-background">
        <div class="container pt-3">
            <nav class="navbar navbar-expand-lg fixed-navbar">
                <div class="container-fluid">
                  <a class="navbar-brand" href="javascript:"><img src="<?php echo base_url();?>assets/images/web-logo.png"></a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto mb-2 mb-lg-0 gap-4">
                            <li class="nav-item">
                                <a class="nav-link active base-sub-info" id="home-nav" aria-current="page" href="javascript:">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link base-sub-info" href="javascript:">Features</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle base-sub-info" href="javascript:" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Pages
                                </a>
                                <ul class="dropdown-menu">
                                <li><a class="dropdown-item base-sub-info" href="javascript:">Action</a></li>
                                <li><a class="dropdown-item base-sub-info" href="javascript:">Another action</a></li>
                                <li><hr class="dropdown-divider"></li>
                                <li><a class="dropdown-item base-sub-info" href="javascript:">Something else here</a></li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link base-sub-info">Support</a>
                            </li>
                        </ul>
                        <form class="d-flex align-items-center gap-4 text-white" role="search" method="post">
                            <img src="<?php echo base_url();?>assets/images/nav-icon.png">
                            <p class="mb-0"><a class="nav-link base-sub-info text-white" href="javascript:">Sign In</a></p>
                            <a href="<?php echo base_url();?>signup.html"class="btn nav-button base-sub-info" type="submit">Sign Up</button></a>
                            <input type="submit" name="logout" class="btn btn-primary px-4" value="Log out">
                        </form>
                    </div>
                </div>
            </nav>
            <div class="service-info">
                <div class="col-12 col-sm-6">
                    <p class="service lh-sm">We specialize in UI/UX, Web Development, Digital Marketing.</p>
                    <p class="service-1 base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque fringilla
                        magna mauris. Nulla fermentum viverra sem eu rhoncus consequat
                        varius nisi quis, posuere magna.</p>
                    <div class="mt-4 row pb-5">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-4 d-flex align-items-center justify-content-center p-0">
                            <button type="button" class="btn btn-primary rounded-pill get-start ps-4 pe-4 text-nowrap ">Get Started Now</button>
                        </div>
                        <div  class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-5 p-0">
                            <p class="m-0 contact-n"><a href="javascript:" class="number"> Call us (0123) 456 – 789</a></p>
                            <p class="m-0 contact-q base-sub-info">For any question or concern</p>
                        </div>    
                    </div>
                </div>
            </div>             
        </div>    
    </div>
    <!----------------------------------------------------------team------------------------------------------------------------>
    <div class="container">
        <div class=" pb-5 support-team">
            <div class="row justify-content-center gap-5">
                <div class="col-12 col-sm-12 col-md-4 col-xl-3 support-details gap-4 p-0">
                    <img src="<?php echo base_url();?>assets/images/support.png">
                    <div class="flex">
                        <h3 class="support">24/7 Support</h3>
                        <p class="support-info base-sub-info">Lorem ipsum dolor sit amet conse adipiscing elit.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-xl-3 support-details gap-4 p-0">
                    <img src="<?php echo base_url();?>assets/images/ownership.png">
                    <div class="flex">
                        <h3 class="support">Take Ownership</h3>
                        <p class="support-info base-sub-info">Lorem ipsum dolor sit amet conse adipiscing elit.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-4 col-xl-3 support-details gap-4 p-0">
                    <img src="<?php echo base_url();?>assets/images/team.png">
                    <div class="flex">
                        <h3 class="support">Team Work</h3>
                        <p class="support-info base-sub-info">Lorem ipsum dolor sit amet conse adipiscing elit.</p>
                    </div>
                </div>
            </div>
        </div>
            <div class="row align-items-center justify-content-center mt-3 gy-5">
                <div class="col-12 col-sm-12 col-lg-6 image-column">
                    <div class="row">
                        <div class="col-2 col-sm-1 p-0 text-end mt-4">
                            <img class="w-75" src="<?php echo base_url();?>assets/images/about-Group.png">
                        </div>
                        <div class="col-12 col-sm-5 col-md-5 p-0 support-img">
                            <img class="support-image support-pic-1" src="<?php echo base_url();?>assets/images/about-01.png.png">
                            <img class="support-pic-2" src="<?php echo base_url();?>assets/images/about-02.png.png">
                        </div>
                        <div class="col-12 col-sm-5 col-md-6 p-0 support-img">
                            <div>
                                <img class="mb-3 support-pic-3" src="<?php echo base_url();?>assets/images/about-Vector.png">
                            </div>
                            <div>
                                <img class="mb-3 image support-pic-4" src="<?php echo base_url();?>assets/images/about-03.png.png">
                            </div>
                            <div>
                                <img class="mb-3 ms-5 support-pic-5" src="<?php echo base_url();?>assets/images/about.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-lg-5">
                    <p class="why-choose">Why Choose Us</p>
                    <h2 class="choose-head base-sub-head">We Make Our customers happy by giving Best services.</h2>
                    <p class="choose-info mt-4 base-sub-info">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum.</p>
                    <div class="mt-4">
                        <button class="btn video-button"><img src="<?php echo base_url();?>assets/images/video-icon.png"></button>
                        <span class="video-info ms-2">SEE HOW WE WORK</span>
                    </div>
                </div>
            </div>
    </div>
    <div class="second-background">
        <div class="container pb-5">
            <div class="text-center pb-5 position-relative">
                <h2 class="team-Dedicated base-sub-head">Meet With Our Creative <br>Dedicated Team</h2>
                <p class="info-Dedicated">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor eros. <br>Donec vitae tortor lacus. Phasellus aliquam ante in maximus.</p>
                <img class="absolute-position" src="<?php echo base_url();?>assets/images/deticated-img-1.png">
                <img class="absolute-pos" src="<?php echo base_url();?>assets/images/deticated-img-2.png">
            </div>
        </div>
    </div>
    <div class="row gap-5 justify-content-center align-items-center team-position">
        <div class="col-12 col-sm-4 team-info">
            <div class="card team-card">
                <img src="<?php echo base_url();?>assets/images/team-01.png.png" >
                <div class="card-body mt-2">
                    <h3 class="team-person">Olivia Andrium</h3>
                    <p class="person-roll base-sub-info">Product Manager</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 team-info">
            <div class="card team-card">
                <img src="<?php echo base_url();?>assets/images/team-02.png.png" >
                <div class="card-body mt-2">
                    <h3  class="team-person">Jemse Kemorun</h3>
                    <p class="person-roll base-sub-info">Product Designer</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-4 team-info">
            <div class="card team-card">
                <img src="<?php echo base_url();?>assets/images/team-03.png.png" >
                <div class="card-body mt-2">
                    <h3  class="team-person">Avi Pestarica</h3>
                    <p class="person-roll base-sub-info">Web Designer</p>
                </div>
            </div>
        </div>
    </div>
    <!----------------------------------------------------Quality service----------------------------------------------------------------------->
    <div class="container">
        <div class="text-center Quality-Service">
            <h1 class="base-sub-head">We Offer The Best Quality Service <br> for You</h1>
            <p class="base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor eros. <br>Donec vitae tortor lacus. Phasellus aliquam ante in maximus.</p>
        </div>
        <div class="row justify-content-center mt-5 pt-3 column-gap-5 row-gap-5">
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/startups.png">
                <h3 class="quality-head">Crafted for Startups</h3>
                <p class="quality-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/high-duality.png">
                <h3 class="quality-head">High-quality Design</h3>
                <p class="quality-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/sections.png">
                <h3 class="quality-head">All Essential Sections</h3>
                <p class="quality-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/speed.png">
                <h3 class="quality-head">Speed Optimized</h3>
                <p class="quality-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/customizable.png">
                <h3 class="quality-head">Fully Customizable</h3>
                <p class="quality-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
            <div class="col-12 col-sm-4 col-md-3">
                <img src="<?php echo base_url();?>assets/images/updates.png">
                <h3 class="quality-head">Regular Updates</h3>
                <p class="quality-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In convallis tortor.</p>
            </div>
        </div>
    </div>
    <div class="third-background mt-5 ">
        <div class="container">
            <div class="d-flex flex-column Premium-Offer">
                <div class="text-center">
                    <h1 class="premium-price base-sub-head">We Offer Great Affordable<br> Premium Prices.</h1>
                    <p class="premium-info base-sub-info">It is a long established fact that a reader will be distracted by the readable content
                        <br>of a page when looking at its layout. The point of using.</p>
                </div>
                <div class="d-flex gap-2 mt-3 gap-3 justify-content-center"> 
                    <div class="d-flex align-items-center">
                        <p class="m-0" >Bill Monthly</p>
                    </div>   
                    <div class="form-check form-switch d-flex align-items-center justify-content-center p-0">
                        <label class="switch">
                            <input id="togglebutton" type="checkbox" onclick="togglecount()">
                            <span id="roundbutton" class="slider round"></span>
                          </label>
                          
                    </div>
                    <div class="d-flex align-items-center">
                        <p class=" m-0" >Bill Annually</p>  
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <div class="container">
        <div class="mt-5 pt-4 text-center">
            <h1 class="premium-price base-sub-head">We Offer Great Affordable<br> Premium Prices.</h1>
            <p class="premium-info mt-3 base-sub-info">It is a long established fact that a reader will be distracted by the readable content<br> of a page when looking at its layout. The point of using.</p>
        </div>
        <div class="premium-tabs mb-5">
            <ul class="nav nav-pills mb-3 justify-content-center gap-3" id="pills-tab" role="tablist">
                <li class="nav-item pt-2 pb-2" role="presentation">
                  <button class="nav-link active ps-4 pe-4 rounded-pill" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true">All</button>
                </li>
                <li class="nav-item pt-2 pb-2" role="presentation">
                  <button class="nav-link ps-4 pe-4 rounded-pill text-nowrap" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false">Branding Strategy</button>
                </li>
                <li class="nav-item pt-2 pb-2" role="presentation">
                  <button class="nav-link ps-4 pe-4 rounded-pill text-nowrap" id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact" aria-selected="false">Digital Experiences</button>
                </li>
                <li class="nav-item pt-2 pb-2" role="presentation">
                  <button class="nav-link ps-4 pe-4 rounded-pill" id="pills-disabled-tab" data-bs-toggle="pill" data-bs-target="#pills-disabled" type="button" role="tab" aria-controls="pills-disabled" aria-selected="false">Ecommerce</button>
                </li>
            </ul>
            <div class="tab-content mt-5" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab" tabindex="0">
                    <div class="row justify-content-center gy-4">
                        <div class="col col-sm-7">
                            <div class="row">
                                <div class="col-12 col-sm-12 ">
                                    <div class="row justify-content-center">
                                        <div class="col-6">
                                            <img class="project-image-1" src="<?php echo base_url();?>assets/images/project-01.png">
                                        </div>
                                        <div class="col-6">
                                            <img class="project-image-2" src="<?php echo base_url();?>assets/images/project-02.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 col-12 col-sm-12 d-flex justify-content-center">
                                    <img class="project-image-3" src="<?php echo base_url();?>assets/images/project-03.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 project-image-mobile">
                            <img class="project-image-4" src="<?php echo base_url();?>assets/images/project-04.png">
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab" tabindex="0"><div>
                    <div class="row justify-content-center gy-4">
                        <div class="col col-sm-7">
                            <div class="row">
                                <div class="col-12 col-sm-12 ">
                                    <div class="row justify-content-center">
                                        <div class="col-6">
                                            <img class="project-image-1" src="<?php echo base_url();?>assets/images/project-01.png">
                                        </div>
                                        <div class="col-6">
                                            <img class="project-image-2" src="<?php echo base_url();?>assets/images/project-02.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 col-12 col-sm-12 d-flex justify-content-center">
                                    <img class="project-image-3" src="<?php echo base_url();?>assets/images/project-03.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 project-image-mobile">
                            <img class="project-image-4" src="<?php echo base_url();?>assets/images/project-04.png">
                        </div>
                    </div>
                </div></div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab" tabindex="0"><div>
                    <div class="row justify-content-center gy-4">
                        <div class="col col-sm-7">
                            <div class="row">
                                <div class="col-12 col-sm-12 ">
                                    <div class="row justify-content-center">
                                        <div class="col-6">
                                            <img class="project-image-1" src="<?php echo base_url();?>assets/images/project-01.png">
                                        </div>
                                        <div class="col-6">
                                            <img class="project-image-2" src="<?php echo base_url();?>assets/images/project-02.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 col-12 col-sm-12 d-flex justify-content-center">
                                    <img class="project-image-3" src="<?php echo base_url();?>assets/images/project-03.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 project-image-mobile">
                            <img class="project-image-4" src="<?php echo base_url();?>assets/images/project-04.png">
                        </div>
                    </div>
                </div></div>
                <div class="tab-pane fade" id="pills-disabled" role="tabpanel" aria-labelledby="pills-disabled-tab" tabindex="0"><div>
                    <div class="row justify-content-center gy-4">
                        <div class="col col-sm-7">
                            <div class="row">
                                <div class="col-12 col-sm-12 ">
                                    <div class="row justify-content-center">
                                        <div class="col-6">
                                            <img class="project-image-1" src="<?php echo base_url();?>assets/images/project-01.png">
                                        </div>
                                        <div class="col-6">
                                            <img class="project-image-2" src="<?php echo base_url();?>assets/images/project-02.png">
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4 col-12 col-sm-12 d-flex justify-content-center">
                                    <img class="project-image-3" src="<?php echo base_url();?>assets/images/project-03.png">
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-4 project-image-mobile">
                            <img class="project-image-4" src="<?php echo base_url();?>assets/images/project-04.png">
                        </div>
                    </div>
                </div></div>
            </div>
        </div>
        <div class="mt-5 pt-5">
            <div class="text-center">
                <h1 class="client-title base-sub-head">Client’s Testimonials</h1>
                <p class="client-intro mb-5 base-sub-info">It is a long established fact that a reader will be distracted by the readable content <br> of a page when looking at its layout. The point of using.</p>
            </div>
        </div>
        <div class="d-flex justify-content-center d-none">
            <div class="client-box mb-5 item position-relative">
                <div class="row justify-content-center pt-5 pb-5 gap-5">
                    <div class="col-12 col-sm-6 col-md-6 col-lg-4 d-flex justify-content-center p-0">
                        <img src="<?php echo base_url();?>assets/images/testimonial.png.png">
                    </div>
                    <div class="col-12 col-sm-6 d-flex align-items-center p-0">
                        <div>
                            <img src="assets/images/double-quotes.png">
                            <p class="client-info mt-4">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In dolor diam, feugiat quis enim sed, ullamcorper semper ligula.
                            Mauris consequat justo volutpat.</p>
                            <h3 class="client-head mt-5">Devid Smith</h3>
                            <a href="javascript:" class="text-decoration-none client-mail">Founter @democompany</a><span class="ms-3"><img src="assets/images/client.png"></span>
                        </div>
                    </div>
                </div>
                <img class="position-absolute top-50 end-0 translate-middle-y" src="<?php echo base_url();?>assets/images/line.png" alt="">
            </div>
        </div>
    </div>
    <div class="container">
        <div class="owl-carousel owl-theme owl-nav-button mb-5">
            <div class="item"><img src="<?php echo base_url();?>assets/images/Carousel.png" ></div>
            <div class="item"><img src="<?php echo base_url();?>assets/images/Carousel.png" ></div>
            <div class="item"><img src="<?php echo base_url();?>assets/images/Carousel.png" ></div>
            <div class="item"><img src="<?php echo base_url();?>assets/images/Carousel.png" ></div>
        </div>
    </div>
    <div class="fourth-backgroud">
        <div class="container">
            <div class="customers">
                <div class="row align-items-center justify-content-center">
                    <div class="col-6 col-sm-6 col-md-3">
                        <p class="customer-count">785</p>
                        <p class="customer-happy base-sub-info">Global Brands</p>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <p class="customer-count">533</p>
                        <p class="customer-happy base-sub-info">Happy Clients</p>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <p class="customer-count">865</p>
                        <p class="customer-happy base-sub-info">Winning Award</p>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <p class="customer-count">346</p>
                        <p class="customer-happy base-sub-info">Happy Clients</p>
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <div  class="container">
        <div class="text-center mt-5">
            <h1 class="global-Brand base-sub-head">Trusted by Global Brands</h1>
            <p class="global-Brand-info base-sub-info">It is a long established fact that a reader will be distracted by the readable content
              <br> of a page when looking at its layout. The point of using.</p>
        </div>
        <div class="row mt-5 pt-4 mb-5 Global-Brands row-gap-5">
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/logitech.png">
            </div>
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/dropcam.png">
            </div>
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/amd.png">
            </div>
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/nike.png">
            </div>
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/mandiri.png">
            </div>
            <div class="col-6 col-sm-6 col-md-3 col-lg-2">
                <img src="<?php echo base_url();?>assets/images/amazon.png">
            </div>
        </div>
        <div class="text-center">
            <h1 class="blogs-head base-sub-head">Latest Blogs & News</h1>
            <p class="blogs-info base-sub-info">It is a long established fact that a reader will be distracted by the readable content<br> of a page when looking at its layout. The point of using.</p>
        </div>
        <div class="row blog-card justify-content-center gy-4">
            <div class="col-12 col-sm-6 col-md-4">
                <div class="card">
                    <img src="<?php echo base_url();?>assets/images/blog-01.png.png" class="card-img-top">
                    <div class="card-body">
                        <span class="me-2 base-sub-info"><img src="<?php echo base_url();?>assets/images/person.png" alt="">Musharof Chy</span><span class="base-sub-info"><img src="assets/images/date.png">25 Dec, 2025</span>
                      <h5 class="card-title">Free advertising for your online business</h5>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="card" >
                    <img src="<?php echo base_url();?>assets/images/blog-02.png.png" class="card-img-top">
                    <div class="card-body">
                        <span class="me-2 base-sub-info"><img src="<?php echo base_url();?>assets/images/person.png" alt="">Musharof Chy</span><span class="base-sub-info"><img src="assets/images/date.png">25 Dec, 2025</span>
                      <h5 class="card-title">9 simple ways to improve your design skills</h5>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4">
                <div class="card">
                    <img src="assets/images/blog-03.png.png" class="card-img-top">
                    <div class="card-body">
                        <span class="me-2 base-sub-info"><img src="<?php echo base_url();?>sassets/images/person.png" alt="">Musharof Chy</span><span class="base-sub-info"><img src="assets/images/date.png">25 Dec, 2025</span>
                      <h5 class="card-title">Tips to quickly improve your coding speed.</h5>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!---------------------------------------------------------------------forms------------------------------------------------------------------->
    <div class="fifth-background mt-5 d-flex align-items-center position-relative">
        <div class="container">
            <div class="company-forms">
                <div class="text-end form-wave-image">
                    <img src="<?php echo base_url();?>assets/images/form-wave.png">
                </div>
                <div class="row justify-content-center gy-4">
                    <div class="company-contacts col-12 col-sm-12 col-md-4">
                        <div class="card">
                            <div class="card-body text-start ms-2">
                                <div class="text-end mt-3 mb-3">
                                    <img class="form-wave" src="<?php echo base_url();?>assets/images/form-wave.png">
                                </div>
                                <h3>Email Address</h3>
                                <p><a href="javascript:" class="base-sub-info">support@startup.com</a></p>
                                <h3>Office Location</h3>
                                <p class="base-sub-info">76/A, Green valle, Califonia USA.</p>
                                <h3>Phone Number</h3>
                                <p><a href="javascript" class="base-sub-info">+009 8754 3433 223</a></p>
                                <h3>Skype Email</h3>
                                <p><a href="javascrpit" class="base-sub-info">example@yourmail.com</a></p>
                                <hr class="hr-line">
                                <h3 class="mb-4">Social Media</h3>
                                <img class="ms-2" src="<?php echo base_url();?>assets/images/facebook.png">
                                <img class="ms-4" src="<?php echo base_url();?>assets/images/twitter.png">
                                <img class="ms-4" src="<?php echo base_url();?>assets/images/linkedin.png">
                                <img class="ms-4" src="<?php echo base_url();?>assets/images/be-social.png">
                                <div class="text-end mt-4">
                                    <img src="<?php echo base_url();?>assets/images/form-d.png">
                                </div>
                              </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-7">
                        <div class="card">
                            <div class="card-body">
                                <form class="row justify-content-center mt-5 gy-4" id="myforms">
                                    <div class="collection-data col-md-5">
                                      <label for="inputfullname" class="form-label">Full name</label>
                                      <input type="text" name="fullname" class="form-control" id="inputfullname"  placeholder="Devid Wonder">
                                    </div>
                                    <div class="collection-data col-md-5">
                                      <label for="inputemail" class="form-label">Email address</label>
                                      <input type="text" name="email" class="form-control" id="inputemail" placeholder="example@gmail.com">
                                    </div>
                                    <div class="collection-data col-md-5">
                                        <label for="inputphonenumber" class="form-label">Phone number</label>
                                        <input type="text" name="phonenumber" class="form-control" id="inputphonenumber" placeholder="+009 3342 3432">
                                    </div>
                                    <div class="collection-data col-md-5">
                                        <label for="inputsubject" class="form-label">Subject</label>
                                        <input type="text" name="subject" class="form-control" id="inputsubject" placeholder="Type your subject">
                                    </div>
                                    <div class="collection-data  col-12 col-sm-12 col-md-10">
                                        <label for="floatingTextarea">Message</label>
                                        <textarea class="form-control" name="message" id="floatingTextarea"></textarea>
                                    </div>                               
                                    <div class="send-message-btn ">
                                      <button type="button" class="btn button-color rounded-pill" id="btnsubmit">Send Message</button>
                                    </div>
                                </form>    
                            </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
        <img class="position-forms" src="<?php echo base_url();?>assets/images/form-u.png">
    </div>
    <div class="backgroud-col">
        <div class="container">
            <div class="row align-items-center startup gy-3">
                <div class="col-12 col-sm-12 col-md-6">
                    <h2 class="join-startups mb-3 base-sub-head">Join with 5000+ Startups Growing with Base.</h2>
                    <p class="startup-info base-sub-info">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis nibh lorem. Duis sed odio lorem. In a efficitur leo. Ut venenatis rhoncus.</p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 text-end start-button">
                    <button type="button" class="btn get-start-btn rounded-pill">Get Started Now</button>
                </div>
            </div>
        </div>
    </div>
        <!------------------------------------------------------------footer-------------------------------------------------------------------------->
    <div class="container">
        <div class="row mt-5 footer gy-5">
            <div class="col-12 col-sm-4 col-lg-3 pe-4">
                <a  href="javascript:"><img class="footer-brand" src="<?php echo base_url();?>assets/images/web-logo.png"></a>
                <p class="mb-4 base-sub-info">Lorem ipsum dolor sit amet, consectetur sadipiscing elit.</p>
                <a href="javascript:"><img class="ms-2" src="<?php echo base_url();?>assets/images/footer-facebook.png"></a>
                <a href="javascript:"><img class="ms-4" src="<?php echo base_url();?>assets/images/footer-twitter.png"></a>
                <a href="javascript:"><img class="ms-4" src="<?php echo base_url();?>assets/images/footer-linkedin.png"></a>
                <a href="javascript:"><img class="ms-4" src="<?php echo base_url();?>assets/images/footer-be.png"></a>              
            </div>
            <div class="col-12 col-sm-4 col-lg-2">
                <h2 class="footer-head">Quick Links</h2>
                <ul class="list-unstyled">
                    <li><a href="javascript:">Home</a></li>
                    <li><a href="javascript:">Product</a></li>
                    <li><a href="javascript:">Careers<span class="btn hiring-button ms-2 text-white"> Hiring</span></a></li>
                    <li><a href="javascript:">Pricing</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-4 col-lg-2">
                <h2 class="footer-head">Services</h2>
                <ul class="list-unstyled">
                    <li><a href="javascript:">Web Development</a></li>
                    <li><a href="javascript:">Graphics Design</a></li>
                    <li><a href="javascript:">Digital Marketing</a></li>
                    <li><a href="javascript:">Ui/Ux Design</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-4 col-lg-2">
                <h2 class="footer-head">Support</h2>
                <ul class="list-unstyled">
                    <li><a href="javascript:">Company</a></li>
                    <li><a href="javascript:">Press media</a></li>
                    <li><a href="javascript:">Our Blog</a></li>
                    <li><a href="javascript:">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-4 col-lg-3">
                <h2 class="footer-head">Newsletter</h2>
                <p class="base-sub-info">Subscribe to receive future updates</p>
                <form>
                    <div class="mb-3 position-relative">
                        <input type="email" class="form-control" id="emailInput" placeholder="Email address">
                        <img class="email-absolute" src="<?php echo base_url();?>assets/images/email-sender.png">
                    </div>
                </form>
            </div>
        </div>
    </div> 
    <hr>
    <div class="container">
        <p class="text-center policy-act base-sub-info">Copyright © 2024 Ayro UI. All Rights Reserved</p>
    </div>  

    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="<?php echo base_url();?>https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.js"></script>
    <script src="<?php echo base_url();?>assets/js/js.js"></script>
</body>
</html>