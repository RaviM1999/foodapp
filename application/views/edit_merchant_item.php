<body>
    <?php 
        // echo "<pre>";
        // print_r($editproduct);
        // exit;
    ?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/add-merchant-item.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Edit Products</h3>
                </div>
            </div>
            <div class="card">
                <div class="row">
                    <div class="col">
                        <div class="title-show">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-3 col-lg-3 col-xl-3 px-0">
                                        <p class="title-1" id="title-1"><span id="no1">01</span>Product Details</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0 dot1-step">
                                        <p class="stp1" id="stp1">- - - - - - - - - - - - </p>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-xl-3 px-0">
                                        <p class="title-2" id="title-2"><span id="no2">02</span>Variant Details</p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0 dot2-step">
                                        <p class="stp2" id="stp2">- - - - - - - - - - - - </p>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-xl-2 px-0">
                                        <p class="title-3" id="title-3"><span id="no3">03</span>View</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="<?php echo base_url(); ?>Merchantitem/update_products" id="form-add-merchant-item" enctype="multipart/form-data" method="post">
                    <div id="form-contents">
                        <div class="container form-contents">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="merchant_name">Merchant Name</label>
                                            <input type="hidden" id="custId" name="product_id" value="<?php echo $editproduct[0]['product_id']?>">
                                            <select class="form-select input-edit" name="product_name" id="merchant_name" aria-label="Default select example">
                                                <option selected><?php echo $editproduct[0]['product_name']?></option>
                                                <option value="kfc">KFC</option>
                                                <option value="Mac Donalds">Mac Donalds</option>
                                                <option value="Chill Out">Chill Out</option>
                                                <option value="Jr.Kuppanna">Jr.Kuppanna</option>
                                                <option value="Mr.Spencer Wise">Mr.Spencer Wise</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="branch">Branch</label>
                                            <select class="form-select input-edit" name="product_branch" id="branch" aria-label="Default select example">
                                                <option selected><?php echo $editproduct[0]['product_branch']?></option>
                                                <option value="Madurai">Madurai</option>
                                                <option value="Chennai">Chennai</option>
                                                <option value="Salem">Salem</option>
                                                <option value="Erode">Erode</option>
                                                <option value="Coimbatore">Coimbatore</option>
                                                <option value="Tiruvannamalai">Tiruvannamalai</option>
                                                <option value="Karur">Karur</option>
                                                <option value="Tiruppur">Tiruppur</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="category">Category</label>
                                            <select class="form-select input-edit" name="product_category" id="category" aria-label="Default select example">
                                                <option selected><?php echo $editproduct[0]['product_category']?></option>
                                                <option value="Burger">Burger</option>
                                                <option value="Biryani">Biryani</option>
                                                <option value="Sandwitch">Sandwitch</option>
                                                <option value="Noodles">Noodles</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="sub_category">Sub Category</label>
                                            <select class="form-select input-edit" name="product_sub_category" id="sub_category" aria-label="Default select example">
                                                <option selected><?php echo $editproduct[0]['product_sub_category']?></option>
                                                <option value="Chicken">Chicken</option>
                                                <option value="Veg">Veg</option>
                                                <option value="Mushroom">Mushroom</option>
                                                <option value="Egg">Egg</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="price">Price</label>
                                            <input type="text" class="form-control input-edit numbersOnly" name="product_price" id="price"
                                                placeholder="Price" value="<?php echo $editproduct[0]['product_price']?>">
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="product">Product</label>
                                            <input type="text" class="form-control input-edit" name="product_nam" id="product"
                                                placeholder="Product" value="<?php echo $editproduct[0]['product_nam']?>">
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-label d-none" for="description">Description</label>
                                            <textarea class="form-control input-edit" name="product_description" id="description" rows="3" placeholder="Description"><?php echo $editproduct[0]['product_description']?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div>
                                        <label class="form-label d-none" for="status">Status</label>
                                        <select class="form-select input-edit" name="product_status" id="status" aria-label="Default select example">
                                            <option selected><?php echo $editproduct[0]['product_status']?></option>
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="image" class="form-label d-none">Select Your Image</label>
                                        <input class="form-control input-edit" name="product_image" id="image" type="file">
                                        <p><?php echo $editproduct[0]['product_image']?></p>
                                        <img class="w-25" src="<?php echo base_url();?>uploads/<?php echo $editproduct[0]['product_image']?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="next-form" id="next-form">
                            <div>
                                <a href="<?php echo base_url();?>merchant_items">
                                    <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                </a>
                            </div>
                            <div>
                                <button type="button" class="btn submit1" name="submit1" id="submit1" value="Next">Next</button>      
                            </div>
                        </div>
                    </div>
                    <div class="d-none" id="form-contents1">
                    <?php
                        foreach ($variantlist as $value) {
                            ?>
                            <div id="variant_form">
                                <div class="container form-contents1 p-0 mx-0" >
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label class="form-label d-none" for="variant">Variant Name</label>
                                                    <select class="form-select input-edit" name="product_var_variant[]" id="variant" aria-label="Default select example">
                                                        <option selected><?php echo $value['product_var_variant']?></option>
                                                        <option value="Regular">Regular</option>
                                                        <option value="Medium">Medium</option>
                                                        <option value="Large">Large</option>
                                                    </select>
                                                </div>
                                                <div  class="col-lg-6">
                                                    <label class="form-label d-none" for="price1">Price</label>
                                                    <input type="text" class="form-control input-edit numbersOnly" name="product_var_price[]" id="price1"
                                                        placeholder="Price" value="<?php echo $value['product_var_price']?>">
                                                </div>
                                                <div  class="col-lg-6">
                                                    <label class="form-label d-none" for="description1">Description</label>
                                                    <textarea class="form-control input-edit" name="product_var_description[]" id="description1" rows="3" placeholder="Description"><?php echo $value['product_var_description']?></textarea>
                                                </div>
                                                <div  class="col-lg-6">
                                                    <label class="form-label d-none" for="status1">Status</label>
                                                    <select class="form-select input-edit" name="product_var_status[]" id="status1" aria-label="Default select example">
                                                        <option selected><?php echo $value['product_var_status']?></option>
                                                        <option value="active">Active</option>
                                                        <option value="inactive">Inactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="row">
                                                <div class="col-lg-10">
                                                    <label for="image1" class="form-label d-none">Select Your Image</label>
                                                    <input class="form-control input-edit" name="product_var_image[]" id="image1" type="file">
                                                    <p><?php echo $value['product_var_image']?></p>
                                                    <img class="w-25" src="<?php echo base_url();?>uploads/<?php echo $value['product_var_image']?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="add_input" class=""></div>
                            </div>
                        <?php }   
                        ?> 
                        <div class="next-form1">
                            <div class="d-flex gap-1 justify-content-center variant-button">
                                <div>
                                    <button type="button" class="btn" name="cancel" id="cancel1">Back</button>
                                </div>
                                <div>
                                    <button type="submit" class="btn submit1" id="submit2" name="submit2">Next</button>        
                                </div>
                            </div>
                            <div>
                                <button type="button" class="btn add-variant d-none" id="add_variant" name="add_variant">+ Add</button>        
                            </div>
                            <div>
                                <button type="button" class="btn d-none" id="remove_variant" name="add_variant">Remove Add</button>
                            </div>
                        </div> 
                    </div>
                </form>              
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-merchant-item").validate({
                rules: {
                    product_name: {
                        required: true,
                    },
                    product_branch: {
                        required: true,
                    },
                    product_status: {
                        required: true,  
                    },
                    product_category: {
                        required: true,  
                    },
                    product_sub_category: {
                        required: true,
                    },
                    product_image: {
                        required: false,
                    },
                    product_price: {
                        required: true,
                        min: 1,
                        number: true,
                    },
                    product_nam: {
                        required: true,
                        minlength: 5,  
                    },
                    product_description: {
                        required: true,
                        minlength: 4,  
                    },
                    product_var_variant: {
                        required: true,
                    },
                    product_var_price: {
                        required: true,
                        min: 1,
                        number: true,
                    },
                    product_var_image: {
                        required: false,
                    },
                    product_var_status: {
                        required: true,
                    },
                    product_var_description: {
                        required: true,
                        minlength: 4,
                    },
                },
                messages: {
                    product_name: {
                        required: "Select Merchant Name",
                    },
                    product_branch: {
                        required: "Select Branch",
                    },
                    product_status: {
                        required: "Enter Status",
                    },
                    product_category: {
                        required: "Select Category",
                    },
                    product_sub_category: {
                        required: "Select Sub Category",
                    },
                    product_image: {
                        required: "Upload Image",
                    },
                    product_price: {
                        required: "Enter Price",
                    },
                    product_nam: {
                        required: "Enter Product",
                    },
                    product_description: {
                        required: "Enter Description",
                    },
                    product_var_variant: {
                        min: "Enter Variant",
                    },
                    product_var_price: {
                        required: "Enter Price",
                    },
                    Product_var_image: {
                        required: "Upload Image",
                    },
                    Product_var_status: {
                        min: "Select Status",
                    },
                    Product_var_description: {
                        required: "Enter Description",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });
            let form1 = document.getElementById("submit1");
            form1.addEventListener("click", firstForm);

            function firstForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents.classList.add("d-none");
                    form1.classList.add("d-none");
                    title2.classList.add("add-title2");
                    stp2.classList.add("add-stp2");
                    title1.classList.add("remove-title1");
                    stp1.classList.add("remove-stp1");
                    no1.classList.add("remove-no1");
                    no2.classList.add("remove-no2");
                    formContents1.classList.remove("d-none")
                }
            }
            
            let cancleForm1 = document.getElementById("cancel1");
            let secondForm = document.getElementById("submit2");
            cancleForm1.addEventListener("click", movefirstForm);
            secondForm.addEventListener("click", thirdForm);
            
            function movefirstForm() {
                formContents.classList.remove("d-none");
                form1.classList.remove("d-none");
                formContents1.classList.add("d-none")
                title2.classList.remove("add-title2");
                stp2.classList.remove("add-stp2");
                title1.classList.remove("remove-title1");
                stp1.classList.remove("remove-stp1");
                no1.classList.remove("remove-no1");
                no2.classList.remove("remove-no2");
            }

            function thirdForm(){
                if ($("#form-add-merchant-item").valid() == true) {
                    formContents1.classList.add("d-none");
                    previewProducts.classList.remove("d-none");
                    title2.classList.remove("add-title2");
                    stp2.classList.remove("add-stp2");
                    no2.classList.remove("remove-no2");
                    title3.classList.add("add-no3");
                    no3.classList.add("add-3");
                }
            }
            
            let cancleForm2 = document.getElementById("cancel2");
            let finalSubmit = document.getElementById("submit3");
            cancleForm2.addEventListener("click", backForm);
            finalSubmit.addEventListener("click", finalform)

            function backForm() {
                previewProducts.classList.add("d-none");
                formContents1.classList.remove("d-none");
                title2.classList.add("add-title2");
                stp2.classList.add("add-stp2");
                no2.classList.add("remove-no2");
                title3.classList.remove("add-no3");
                no3.classList.remove("add-3");
            }

            function finalform() {
                if ($("#form-add-merchant-item").valid() == true) {
                    $("#form-add-merchant-item").submit();
                }
            }
        });
        
        let formContents =document.getElementById("form-contents");
        let formContents1 = document.getElementById("form-contents1");
        let previewProducts = document.getElementById("preview-products");
        let title1 = document.getElementById("title-1");
        let stp1 = document.getElementById("stp1");
        let title2 = document.getElementById("title-2");
        let stp2 = document.getElementById("stp2");
        let title3 = document.getElementById("title-3");
        let no1 = document.getElementById("no1");
        let no2 = document.getElementById("no2");
        let no3 = document.getElementById("no3");

        let control = document.getElementById("variant_form").innerHTML;
        let addinput = document.getElementById("add_input");
        // console.log(control);
        document.getElementById("add_variant").addEventListener("click", addfunction);
        function addfunction() {
            if (document.getElementById('add_input').classList.contains("d-none")) {
                addinput.classList.remove("d-none");
            } else {
                document.getElementById("add_input").innerHTML += control;
            }
                        
        }
        document.getElementById("remove_variant").addEventListener("click", removefunction);
        function removefunction() {
            addinput.classList.add("d-none");
        }
        
    </script>
</body>
