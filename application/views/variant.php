<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/variant.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <p class="home-text d-none">Home <span>/ Merchant Items</span></p>
        <h3>Variants</h3>
        <div class="card">
            <div class="row p-4">
                <form class="tittle-box-form2 col-lg-10" role="search">
                    <div class="row">
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="Search" aria-label="Search">                                             
                        </div>
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="From Date" aria-label="Search">                                            
                        </div>
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="To Date" aria-label="Search">                                            
                        </div>
                        <div class="col-6 col-lg-2">
                            <select class="form-select input-edit-title" id="status" name="status" aria-label="Default select example">
                                <option selected>Status</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>                                            
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                        </div>
                    </div>
                </form>
                <div class="col-lg-2">
                        <a type="button" class="btn add-btn" href="#" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">
                            + Add
                        </a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-borderless text-left">
                    <thead>
                        <th>ID</th>
                        <th>Variants</th>
                        <th>Variant Type</th>
                        <th>Value</th>
                        <th class="text-center">Created Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        <?php
                            $i=1; 
                            foreach ($variants as $variant_item) {
                                ?>
                                <tr>
                                    <td><?php echo $i?></td>
                                    <td><?php echo $variant_item->variant_name ?></td>
                                    <td><?php echo $variant_item->variant_type ?></td>
                                    <td><?php echo $variant_item->variant_value ?></td>
                                    <td class="text-center"><?php echo $variant_item->date ?></td>
                                    <td class="text-center <?php echo ($variant_item->variant_status == "active")?'tbl-active':''?>"><?php echo $variant_item->variant_status ?></td>
                                    <td class="text-center">
                                        <div class="dropdown">
                                            <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                <button>...</button>
                                            </a>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" data-bs-target="#variantModalToggle<?php echo $variant_item->variant_id ?>" data-bs-toggle="modal" href="#"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                                <li><a class="dropdown-item" href="<?php echo base_url()?>Variants_con/variant_delete?<?php echo $variant_item->variant_id ?>"><i class="material-icons text-center">&#xe254;</i>delete</a></li>
                                                <li><a class="dropdown-item" href="<?php echo base_url()?>Variants_con/variant_edit?variant_id=<?php echo $variant_item->variant_id ?>"><i class="material-icons text-center">&#xe872;</i>Edit</a></li>
                                            </ul>
                                        </div> 
                                    </td> 
                                </tr>
                                <div class="customise-modal">
                                    <div class="modal fade" id="variantModalToggle<?php echo $variant_item->variant_id ?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="total-view">
                                                        <h1>Add Variants</h1>
                                                        <div class="d-flex justify-content-end">
                                                            <button type="button" class="btn" name="edit" id="edit1">Edit</button>
                                                        </div>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-6 col-md-5 col-lg-3">
                                                                    <p>Variant Name</p>
                                                                    <p>Variant Type</p>
                                                                    <p>Value</p>
                                                                </div>
                                                                <div class="col-6 col-md-5 col-lg-3 sec2-list">
                                                                    <p><?php echo $variant_item->variant_name ?></p>
                                                                    <p><?php echo $variant_item->variant_type ?></p>
                                                                    <p><?php echo $variant_item->variant_value ?></p>
                                                                </div>
                                                                <div class="col-6 col-md-5 col-lg-3">
                                                                    <p>Created date</p>
                                                                    <p>Status</p>
                                                                </div>
                                                                <div class="col-6 col-md-5 col-lg-3 sec2-list">
                                                                    <p><?php echo $variant_item->date ?></p>
                                                                    <p class="<?php echo ($variant_item->variant_status == "active")?'tbl-active':''?>"><?php echo $variant_item->variant_status ?></p>
                                                                </div>
                                                            </div>
                                                            <div class="final-submit d-flex justify-content-center mt-4 mb-4">
                                                                <div>
                                                                    <button type="button" class="btn" name="cancel1" id="cancel1">
                                                                    Cancel
                                                                </button>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php $i++; }
                            ?>
                    </tbody>
                </table>
            </div>                         
            <div class="d-flex justify-content-end mx-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                               <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="customise-modal">
        <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <div id="modal-form-edit">
                            <h1>Add Variants</h1>
                            <form action="<?php echo base_url();?>Variants_con/variants_add" enctype="multipart/form-data" id="form-add-variant" method="post">
                                <div class="modal-form-edit">
                                    <div>
                                        <label class="form-label d-none" for="variant">Variant</label>
                                        <input type="text" class="form-control" name="variant_name" id="variant"
                                            placeholder="Variant Name">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="varianttype">Variant</label>
                                        <input type="text" class="form-control" name="variant_type" id="varianttype"
                                            placeholder="Variant Type">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="variantvalue">Value</label> 
                                        <input type="text" class="form-control numbersOnly" name="variant_value" id="variantvalue"
                                            placeholder="Variant Value">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="status">Status</label>
                                        <select class="form-select" name="variant_status" id="status" aria-label="Default select example">
                                            <option disabled selected>Status</option>
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-submit">
                                    <div>
                                        <a href="<?php echo base_url();?>variants">
                                            <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                        </a>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn" name="submit" id="submit">
                                            Save
                                        </button>        
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
              </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-variant").validate({
                rules: {
                    variant_name: {
                        required: true,
                    },
                    variant_type: {
                        required: true,
                    },
                    variant_value: {
                        required: true,
                    },
                    variant_status: {
                        required: true,
                    },
                   
                },
                messages: {
                    variant_type: {
                        required: "Enter Variant",
                    },
                    variant_type: {
                        required: "Enter Variant Type",
                    },
                    variant_status: {
                        min: "Select Value",
                    },
                    variant_status: {
                        min: "Select Status",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            submit.addEventListener("click", hidef);

            function hidef() {
                if ($("#form-add-variant").valid() == true) {
                    maincontent.classList.add("d-none");
                    totalView.classList.remove("d-none");
                }
            }

            cancel.addEventListener("click", showf);

            function showf() {
                maincontent.classList.remove("d-none");
                totalView.classList.add("d-none");
            }
        });
        maincontent = document.getElementById("modal-form-edit");
        totalView = document.getElementById("total-view");
        submit = document.getElementById("submit");
        cancel = document.getElementById("cancel1");

    </script>
</body>
</html>