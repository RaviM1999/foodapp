<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/variant.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="customise-modal">
            <div class="modal-dialog model-items">
                <div class="modal-content bg-white model-box">
                    <div class="modal-body">
                        <div id="modal-form-edit">
                            <h1 class="mt-4">Edit Variants</h1>
                            <form action="<?php echo base_url();?>Variants_con/variant_update" enctype="multipart/form-data" id="form-add-variant" method="post">
                                <div class="modal-form-edit ms-4">
                                    <div>
                                        <label class="form-label d-none" for="variant">Variant</label>
                                        <input type="hidden" id="custId" name="variant_id" value="<?php echo $variant_value[0]['variant_id']?>">
                                        <input type="text" class="form-control" name="variant_name" id="variant"
                                            placeholder="Variant Name" value="<?php echo $variant_value[0]['variant_name']?>">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="varianttype">Variant</label>
                                        <input type="text" class="form-control" name="variant_type" id="varianttype"
                                            placeholder="Variant Type" value="<?php echo $variant_value[0]['variant_type']?>">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="variantvalue">Value</label> 
                                        <input type="text" class="form-control numbersOnly" name="variant_value" id="variantvalue"
                                            placeholder="Variant Value" value="<?php echo $variant_value[0]['variant_value']?>">
                                    </div>
                                    <div>
                                        <label class="form-label d-none" for="status">Status</label>
                                        <select class="form-select" name="variant_status" id="status" aria-label="Default select example">
                                            <option selected><?php echo $variant_value[0]['variant_status']?></option>
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-submit">
                                    <div>
                                        <a href="<?php echo base_url()?>variants">
                                            <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                        </a>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn" name="submit" id="submit">
                                            Save
                                        </button>        
                                    </div>
                                </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-variant").validate({
                rules: {
                    variant_name: {
                        required: true,
                    },
                    variant_type: {
                        required: true,
                    },
                    variant_value: {
                        required: true,
                    },
                    variant_status: {
                        required: true,
                    },
                   
                },
                messages: {
                    variant_type: {
                        required: "Enter Variant",
                    },
                    variant_type: {
                        required: "Enter Variant Type",
                    },
                    variant_status: {
                        min: "Select Value",
                    },
                    variant_status: {
                        min: "Select Status",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            submit.addEventListener("click", hidef);

            function hidef() {
                if ($("#form-add-variant").valid() == true) {
                    maincontent.classList.add("d-none");
                    totalView.classList.remove("d-none");
                }
            }

            cancel.addEventListener("click", showf);

            function showf() {
                maincontent.classList.remove("d-none");
                totalView.classList.add("d-none");
            }
        });
        maincontent = document.getElementById("modal-form-edit");
        totalView = document.getElementById("total-view");
        submit = document.getElementById("submit");
        cancel = document.getElementById("cancel1");
    </script>
</body>
</html>