<?php $this->load->view("includes/header");?>
<body>
    <title>Login</title>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/admin-login.css">
    <div class="container">
        <div class="row justify-content-center align-items-center vh-100">
            <div class="card col-sm-10 col-md-8 col-lg-6 rounded-5 border-0 text-center p-3">
                <img src="<?php echo base_url();?>assets/images/logo.png" class="w-25 mx-auto mb-3">
                <form action="" class="row justify-content-center login-form" method="post">
                    <?php echo validation_errors(); ?> 
                    <h2 class="col-9 bold-orange text-start">Login</h2>
                    <div class="col-9 mb-3 text-start">
                        <label for="email" class="form-label form-edit-input">Email address</label>
                        <input type="text" name="email" id="email" class="form-control form-edit-input" placeholder="Enter Email">                    
                    </div>
                    <div class="col-9 mb-3 text-start">
                        <label for="password" class="form-label form-edit-input">Password</label>
                        <input type="password" name="password" id="password" class="form-control form-edit-input" placeholder="Enter Password">
                    </div>
                    <div class="row justify-content-center">
                        <input type="submit" name="login" class="btn orange-btn rounded-5 col-3 col-sm-2" value="Login">
                    </div>                      
                </form>
                <a href="<?php echo base_url(); ?>register" class="link-text mt-2"><span>New to here ? Signup here.</span></a>
            </div>
        </div>
    </div>
</body>
<?php $this->load->view("includes/footer") ;?>
