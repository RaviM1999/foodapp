<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sub-category.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <p class="home-text d-none">Home <span>/ Merchant Items</span></p>
        <h3>Sub Category</h3>
        <div class="card">
            <div class="row p-4">
                <form class="tittle-box-form2 col-lg-10" role="search">
                    <div class="row">
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="Search" aria-label="Search">                                             
                        </div>
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="From Date" aria-label="Search">                                            
                        </div>
                        <div class="col-6 col-lg-2">
                            <input class="form-control input-edit-title" type="search" placeholder="To Date" aria-label="Search">                                            
                        </div>
                        <div class="col-6 col-lg-2">
                            <select class="form-select input-edit-title" id="status" name="status" aria-label="Default select example">
                                <option selected>Status</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>                                            
                        </div>
                        <div class="col-lg-2">
                            <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                            <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                        </div>
                    </div>
                </form>
                <div class="col-lg-2">
                        <a type="button" class="btn add-btn" href="#" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">+ Add</a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-borderless text-left">
                    <thead>
                        <th>ID</th>
                        <th>Merchant</th>
                        <th>Branch</th>
                        <th>Category</th>
                        <th class="w-25">Sub Category</th>
                        <th class="text-center text-nowrap">Created Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        <?php $i=1; foreach ($sub_category_data as $sub_category) { ?>
                            <tr>
                                <td><?php echo $i ?></td>
                                <td class="text-nowrap"><?php echo $sub_category->sub_category_name?></td>
                                <td><?php echo $sub_category->sub_category_branch?></td>
                                <td><?php echo $sub_category->sub_category?></td>
                                <td class="px-0"><img class="me-3 product-image" src="<?php echo base_url();?>uploads/<?php echo $sub_category->sub_category_image?>"><?php echo $sub_category->sub_category_list?></td>
                                <td class="text-center"><?php echo $sub_category->date?></td>
                                <td class="text-center <?php echo ($sub_category->sub_category_status == 'active')?'tbl-active':''?>"><?php echo $sub_category->sub_category_status?></td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <button>...</button>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" data-bs-target="#buttonModalToggle<?php echo $sub_category->sub_category_id?>" data-bs-toggle="modal" href="#"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Subcategory/delete_sub_category?<?php echo $sub_category->sub_category_id?>"><i class="material-icons text-center">&#xe254;</i>delete</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url()?>Subcategory/edit_sub_category?sub_category_id=<?php echo $sub_category->sub_category_id?>"><i class="material-icons text-center">&#xe872;</i>Edit</a></li>
                                        </ul>
                                    </div> 
                                </td> 
                            </tr>
                            <div class="customise-modal">
                                <div class="modal fade" id="buttonModalToggle<?php echo $sub_category->sub_category_id?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body p-4">
                                                <div class="total-view">
                                                    <h1 class="">View Sub Category Details</h1>
                                                    <div class="d-flex justify-content-end">
                                                        <button type="button" class="btn" name="edit" id="edit1">Edit</button>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-10 col-sm-10 col-md-10 col-lg-3 px-0">
                                                                <img class="img-thumbnail" src="<?php echo base_url();?>uploads/<?php echo $sub_category->sub_category_image?>">
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-4 col-lg-3 px-0">
                                                                <h2>Chicken Burger</h2>
                                                                <p>Merchant Name</p>
                                                                <p>Branch Name</p>
                                                                <p>Category Name</p>
                                                                <p>Description</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-4 col-lg-2 sec2-list px-0">
                                                                <p><?php echo $sub_category->sub_category_name?></p>
                                                                <p><?php echo $sub_category->sub_category_branch?></p>
                                                                <p><?php echo $sub_category->sub_category?>r</p>
                                                                <p>Crumbled tofu pressed together with lemongrass, sriracha</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec3-list px-0">
                                                                <p>Created Date</p>
                                                                <p>Status</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec4-list px-0">
                                                                <p><?php echo $sub_category->date?></p>
                                                                <p class="<?php echo ($sub_category->sub_category_status == 'active')?'tbl-active':''?>"><?php echo $sub_category->sub_category_status?></p>
                                                            </div>
                                                        </div>
                                                        <div class="final-submit d-flex justify-content-center" id="final-submit">
                                                            <a href="<?php echo base_url()?>sub_category">
                                                                <button type="button" class="btn" name="cancel12" id="cancel12">Cancel</button>
                                                            </a> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } ?>  
                    </tbody>
                </table>
            </div>
            <div class="d-flex justify-content-end mx-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="customise-modal">
        <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body p-4">
                        <h1 id="tle-1">Add Sub Category</h1>
                        <form action="<?php echo base_url();?>Subcategory/sub_category" enctype="multipart/form-data" id="form-add-sub-category" method="post">
                            <div class="modal-form-edit row" id="modal-form-edit">
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="merchant">Merchant Name</label>
                                    <select class="form-select" name="sub_category_name" id="merchant" onchange="categoryfunction(this)" aria-label="Default select example">
                                        <option disabled selected>Merchant Name</option>
                                        <?php foreach ($merchant_data as $list) { ?>
                                                <option data-id="<?php echo $list['merchant_id']?>" value="<?php echo $list['merchant_restaurant_name']?>"><?php echo $list['merchant_restaurant_name']?></option> 
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="branch">Branch Name</label>
                                    <select class="form-select" name="sub_category_branch" id="sub_category_branch" aria-label="Default select example">
                                        <option disabled selected>Branch</option>
                                        
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="form-label d-none" for="category">Category</label>
                                    <select class="form-select" name="sub_category" id="category" aria-label="Default select example">
                                        <option disabled selected>Category</option>
                                        <?php foreach ($category_data as $category) { ?>
                                            <option value="<?php echo $category->category_category?>"><?php echo $category->category_category?></option> 
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="subcategory">Sub Category</label>
                                    <input type="text" class="form-control" name="sub_category_list" id="subcategory"
                                        placeholder="Sub Category Name">
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="status">Status</label>
                                    <select class="form-select" name="sub_category_status" id="status" aria-label="Default select example">
                                        <option disabled selected>Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="description">Description</label>
                                    <textarea class="form-control" name="sub_category_description" id="description" rows="3" placeholder="Description"></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="image" class="form-label d-none">Select Your Image</label>
                                    <input class="form-control" name="sub_category_image" id="image" type="file">
                                </div>
                            </div>
                            <div class="modal-submit" id="modal-submit">
                                <div>
                                    <a href="<?php echo base_url();?>sub_category">
                                        <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                    </a>
                                </div>
                                <div>
                                    <button type="submit" class="btn" name="submit" id="submit">Save</button>        
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-sub-category").validate({
                rules: {
                    sub_category_name: {
                        required: true,
                    },
                    sub_category_branch: {
                        required: true,
                    },
                    sub_category: {
                        required: true,
                    },
                    sub_category_list: {
                        required: true,
                    },
                    sub_category_status: {
                        required: true,
                    },
                    sub_category_image: {
                        required: true,
                    },
                    sub_category_description: {
                        required: true,
                    },
                    
                },
                messages: {
                    sub_category_name: {
                        min: "Select Your Merchant",
                    },
                    sub_category_branch: {
                        min: "Select Your Branch",
                    },
                    sub_category: {
                        min: "Select Your Category",
                    },
                    sub_category_list: {
                        required: "Enter Sub Category",
                    },
                    sub_category_status: {
                        min: "Select Status",
                    },
                    sub_category_image: {
                        required: "Upload Image",
                    },
                    sub_category_description: {
                        required: "Enter Description",
                    },
                }
            });

            form1sub = document.getElementById("submit");
            form1sub.addEventListener("click",hide1);

            function hide1() {
                if ($("#form-add-sub-category").valid() == true) {
                    tle1.classList.add("d-none");
                    form1.classList.add("d-none");
                    form1close.classList.add("d-none");
                    totalView.classList.remove("d-none");
                    finalSubmit.classList.remove("d-none");
                 }
             }
            firstForm = document.getElementById("cancel12");
            firstForm.addEventListener("click", show1);

            function show1() {
                tle1.classList.remove("d-none");
                form1.classList.remove("d-none");
                form1close.classList.remove("d-none");
                totalView.classList.add("d-none");
                finalSubmit.classList.add("d-none");
            }
        });
        function categoryfunction(data) {
            let opt = data.options[data.selectedIndex];
            let id = opt.dataset.id
            
            console.log(id);
                let xhr = new XMLHttpRequest();
                let url = 'Merchantcategory/category_data/'+id;
                xhr.open("GET", url, true);
                xhr.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        let category_branch = JSON.parse(this.responseText);
                        console.log(typeof(category_branch));
                        let branch = `<option value ="">Select Branch</option>`;
                        category_branch.forEach((index,value,key) => {
                            console.log(index);
                            branch += `<option value ="${index}">${index}</option>`
                        });
                        document.getElementById("sub_category_branch").innerHTML = branch;
                    }
                }
                xhr.send();
        }
    </script>
</body>