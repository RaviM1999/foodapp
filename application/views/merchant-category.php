<body>
    <?php 
        // echo "<pre>";
        // print_r($merchant_branchs);
        // exit;
    ?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin-merchant-category.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <p class="home-text d-none">Home<span>/Merchant</span></p>
        <h3>Merchant category</h3>
        <div class="card">
            <div class="container tittle-box p-4">
                <div class="row">
                    <form class="tittle-box-form col-lg-10" role="search">
                        <div class="row">
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="Search" aria-label="Search">  
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="From Date" aria-label="Search">
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="To Date" aria-label="Search">
                            </div>
                            <div class="col-6 col-md-2">
                                <input class="form-control title-form-edit" type="search" placeholder="Status" aria-label="Search"> 
                            </div>
                            <div class="col-md-2">
                                <button type="button" class="btn" name="cancel" id="cancel134"><i class="material-icons">&#xe5d5;</i></button>
                                <button type="button" class="btn" name="cancel" id="cancel165"><i class="material-icons">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2 text-end">
                        <button type="button" class="btn" href="#" data-bs-target="#exampleModalToggle" data-bs-toggle="modal">+ Add</button>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-borderless text-left">
                    <thead>
                        <th>ID</th>
                        <th>Merchant</th>
                        <th>Branch</th>
                        <th class="w-25">Category</th>
                        <th class="text-center text-nowrap">Offer Percentage</th>
                        <th class="text-center text-nowrap">Offer Applied</th>
                        <th class="text-center text-nowrap">Created Date</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                    </thead>
                    <tbody>
                        <?php $i = 1; foreach ($category_list as $result) { ?>
                            <tr>
                                <td><?php echo $i;?></td>
                                <td class="text-nowrap"><?php echo $result->category_name?></td>
                                <td><?php echo $result->category_branch?></td>
                                <td><img class="me-2 product-image" src="<?php echo base_url();?>uploads/<?php echo $result->category_image?>"> <?php echo $result->category_category?></td>
                                <td class="text-center text-nowrap"><?php echo $result->category_offer_percentage?>%</td>
                                <td class="text-center text-nowrap"><?php echo $result->category_offer_percentage?>%</td>
                                <td class="text-center text-nowrap"><?php echo $result->date?></td>
                                <td class="text-center <?php echo ($result->category_status == 'active')?'tbl-active':''?>"><?php echo $result->category_status?></td>
                                <td class="text-center">
                                    <div class="dropdown">
                                        <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                            <button>...</button>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" data-bs-target="#modalTogglebutton<?php echo $result->category_id?>" data-bs-toggle="modal" href="#"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Merchantcategory/delete_data?<?php echo $result->category_id?>"><i class="material-icons text-center">&#xe872;</i>delete</a></li>
                                            <li><a class="dropdown-item" href="<?php echo base_url();?>Merchantcategory/edit_category?<?php echo $result->category_id?>"><i class="material-icons text-center">&#xe254;</i>Edit</a></li>
                                        </ul>
                                    </div> 
                                </td> 
                            </tr>
                            <div class="customise-modal">
                                <div class="modal fade" id="modalTogglebutton<?php echo $result->category_id?>" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="total-view" id="total-view">
                                                    <h1 class="">View Category Details</h1>
                                                    <div class="d-flex justify-content-end">
                                                        <a href="<?php echo base_url();?>Merchantcategory/edit_category?category_id=<?php echo $result->category_id?>">
                                                            <button type="button" class="btn" name="edit" id="edit1">Edit</button>
                                                        </a>
                                                    </div>
                                                    <div class="container">
                                                        <div class="row">
                                                            <div class="col-12 col-sm-12 col-md-12 col-lg-3 px-0">
                                                                <img class="w-75" src="<?php echo base_url();?>uploads/<?php echo $result->category_image?>">
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-4 col-lg-3 px-0">
                                                                <h2><?php echo $result->category_category?></h2>
                                                                <p>Merchant Name</p>
                                                                <p>Branch Name</p>
                                                                <p>Description</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-4 col-lg-2 sec2-list px-0">
                                                                <p><?php echo $result->category_name?></p>
                                                                <p><?php echo $result->category_branch?></p>
                                                                <p>Crumbled tofu pressed together with lemongrass, sriracha</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec3-list px-0">
                                                                <p>Offer Percentage</p>
                                                                <p>Offer Enabled</p>
                                                                <p>Created Date</p>
                                                                <p>Status</p>
                                                            </div>
                                                            <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec4-list px-0">
                                                                <p><?php echo $result->category_offer_percentage?></p>
                                                                <p><i class="material-icons text-center">&#xe5ca;</i></p>
                                                                <p><?php echo $result->date?></p>
                                                                <p class="<?php echo ($result->category_status == 'active')?"tbl-active":""?>"><?php echo $result->category_status?></p>
                                                            </div>
                                                        </div>
                                                        <div class="review-item" id="review-item">
                                                            <div class="d-none">
                                                                <a href="<?php echo base_url();?>category_merchant">
                                                                    <button type="button" class="btn" name="cancel1" id="cancel1">Cancel</button>
                                                                </a>            
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; } ?> 
                    </tbody>
                </table>
                
            </div>
            <div class="d-flex justify-content-end mx-4">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                        </li>
                     </ul>
                </nav>
            </div>
        </div>
    </div>
    <div class="customise-modal">
        <div class="modal fade" id="exampleModalToggle" aria-hidden="true" aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-body">
                        <h1 class="title-1" id="title-1">Add Category</h1>
                        <form action="<?php echo base_url()?>Merchantcategory/category_merchant" enctype="multipart/form-data" id="form-add-merchant-category" method="post">
                            <div class="modal-form-edit row justify-content-between" id="modal-form-edit">
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="merchant">Product Name</label>
                                    <select class="form-select input-edit" name="category_name" id="merchant" onchange="categoryfunction(this)" aria-label="Default select example">
                                        <option disabled selected>Merchant Name</option>
                                        <?php  
                                            foreach ($merchant_data as $list) {
                                                ?>
                                                <option data-price="<?php echo $list['merchant_id']?>" value="<?php echo $list['merchant_restaurant_name']?>"><?php echo $list['merchant_restaurant_name']?></option> 
                                        <?php } 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="branch">Branch</label>
                                    <select class="form-select input-edit" name="category_branch" id="category_branch" aria-label="Default select example">
                                        <option disabled selected>Branch</option>
                                        
                                    </select>
                                </div>
                                <div class="form-check check-bow-edit col-lg-3">
                                    <input class="form-check-input" type="checkbox" name="offer_checkbox" id="flexCheckChecked" value="" checked>
                                    <label class="form-check-label" for="flexCheckChecked">Is offer enabled</label>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="category">Category</label>
                                    <input type="text" class="form-control input-edit" name="category_category" id="category"
                                        placeholder="Category Name">
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="offerpercentage">Offer Percentage</label>
                                    <input type="text" class="form-control input-edit numbersOnly" name="category_offer_percentage" id="offerpercentage"
                                        placeholder="Offer Percentage">
                                </div>
                                <div class="col-lg-3 px-0">
                                    <label class="form-label d-none" for="status">Status</label>
                                    <select class="form-select input-edit" name="category_status" id="status" aria-label="Default select example">
                                        <option disabled selected>Status</option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="col-12 pe-0">
                                    <label class="form-label d-none" for="description">Description</label>
                                    <textarea class="form-control input-edit" name="category_description" id="description" rows="3" placeholder="Description"></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="image" class="form-label d-none">Select Your Image</label>
                                    <input class="form-control input-edit" name="category_image" id="image" type="file">
                                </div>
                            </div>
                            <div class="modal-submit" id="modal-submit"> 
                                <div>
                                    <a href="<?php echo base_url();?>category_merchant">
                                        <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                    </a>
                                </div>
                                <div>
                                    <button type="submit" class="btn" name="submit" id="submit1">Save</button>        
                                </div>
                            </div> 
                        </form>
                        <div class="d-none">
                            <div class="total-view d-none">
                                <h1 class="">View Category Details</h1>
                                <div class="d-flex justify-content-end">
                                    <button type="button" class="btn" name="edit" id="edit1">Edit</button>
                                </div>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-3 px-0">
                                            <img src="<?php echo base_url();?>assets/images/burger.png">
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-4 col-lg-3 px-0">
                                            <h2>Chicken Burger</h2>
                                            <p>Merchant Name</p>
                                            <p>Branch Name</p>
                                            <p>Description</p>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-4 col-lg-2 sec2-list px-0">
                                            <p>Mr.Spencer Wise</p>
                                            <p>Chennai</p>
                                            <p>Crumbled tofu pressed together with lemongrass, sriracha</p>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec3-list px-0">
                                            <p>Offer Percentage</p>
                                            <p>Offer Enabled</p>
                                            <p>Created Date</p>
                                            <p>Status</p>
                                        </div>
                                        <div class="col-6 col-sm-6 col-md-2 col-lg-2 sec4-list px-0">
                                            <p>20 %</p>
                                            <p><i class="material-icons text-center">&#xe5ca;</i></p>
                                            <p>22/04/23</p>
                                            <p class="tbl-active">Active</p>
                                        </div>
                                    </div>
                                    <div class="review-item d-none" id="review-item">
                                        <div>
                                            <button type="button" class="btn" name="cancel1" id="cancel1">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                 </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>

        $(document).ready(function(){
            $("#form-add-merchant-category").validate({
                rules: {
                    category_name: {
                        required: true,
                    },
                    category_branch: {
                        required: true,
                    },
                    category_category: {
                        required: true,
                    },
                    category_offer_percentage: {
                        required: true,
                    },
                    category_status: {
                        required: true,
                    },
                    category_description: {
                        required: true,
                    },
                    category_image: {
                        required: true,
                    },
                    
                },
                messages: {
                    category_name: {
                        min: "Select Your Merchant",
                    },
                    category_branch: {
                        required: "Enter Your Branch",
                    },
                    category_category: {
                        required: "Enter Category",
                    },
                    category_offer_percentage: {
                        required: "Enter Offer Percentage",
                    },
                    category_status: {
                        min: "Select Status",
                    },
                    category_description: {
                        required: "Enter Description",
                    },
                    category_image: {
                        required: "Upload Image",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });

            Submit = document.getElementById("submit1");
            Submit.addEventListener("click", secondForm);

            function secondForm() {
                if ($("#form-add-merchant-category").valid() == true) {
                    title1.classList.add("d-none");
                    modalForm.classList.add("d-none");
                    totalView.classList.remove("d-none");
                    reviewItem.classList.remove("d-none");
                    modalSubmit.classList.add("d-none");
                }
            }
            cancel1 = document.getElementById("cancel1");
            cancel1.addEventListener("click", firstForm);

            function firstForm() {
                title1.classList.remove("d-none");
                modalForm.classList.remove("d-none");
                totalView.classList.add("d-none");
                reviewItem.classList.add("d-none");
                modalSubmit.classList.remove("d-none");
            }
        });
    title1 = document.getElementById("title-1");
    totalView = document.getElementById("total-view");
    modalSubmit = document.getElementById("modal-submit");    
    modalForm = document.getElementById("modal-form-edit");   
    reviewItem = document.getElementById("review-item");

    function categoryfunction(data) {
        let opt = data.options[data.selectedIndex];
        let price = opt.dataset.price
        
        console.log(price);
            let xhr = new XMLHttpRequest();
            let url = 'Merchantcategory/category_data/'+price;
            xhr.open("GET", url, true);
            xhr.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    let category_branch = JSON.parse(this.responseText);
                    console.log(typeof(category_branch));
                    let branch = `<option value ="">Branch</option>`;
                    category_branch.forEach((index,value,key) => {
                        console.log(index);
                        branch += `<option value ="${index}">${index}</option>`
                    });
                    document.getElementById("category_branch").innerHTML = branch;
                }
            }
            xhr.send();
    }
    </script>
</body>
