<?php 
    // echo "<pre>";
    // print_r($branch_value);
    // exit;
?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/view-merchant-details.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row justify-content-between my-3 me-4">
                <h3 class="col head-font">View Merchant Details</h3>
                <div class ="col-2">
                    <a href="<?php echo base_url();?>Merchant/edit_merchant?merchant_id=<?php echo  $merchant_val[0]['merchant_id']?>">
                        <button class=" button-orange">Edit</button>
                    </a>
                </div>
            </div>
            <div class="card">
                <div class="card display-content-card p-4">
                    <div class="row">
                        <div class="col-lg-3">
                            <img src="<?php echo base_url(); ?>uploads/<?php echo $merchant_val[0]['merchant_logo'] ?>" class="img-fluid">
                        </div>
                        <div class="col">
                            <div class="row mt-4 ps-2">
                                <h3 class="col-3 head-font"><?php echo $merchant_val[0]['merchant_restaurant_name'] ?></h3>
                                <h3 class="col-3 head-font-blur">3.5/5<h3>
                            </div>
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <table class="table table-borderless mb-0">
                                        <tr>
                                            <td class="col-6 py-1">Contact Name</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['merchant_contact_name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Phone Number</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['merchant_phone_number'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Email Id</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['merchant_email'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Conatct Address</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['merchant_contact_address'] ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <table class="table table-borderless">
                                        <tr>
                                            <td class="col-6 py-1">Merchant Id</td>
                                            <td class="col-6 py-1">#MR0<?php echo $merchant_val[0]['merchant_id'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Registration Date</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['date'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Website</td>
                                            <td class="col-6 py-1"><?php echo $merchant_val[0]['merchant_website'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="col-6 py-1">Status</td>
                                            <td class="col-6 py-1 tbl-active"><?php echo $merchant_val[0]['merchant_status'] ?></td>
                                        </tr>
                                    </table>
                                </div>                                    
                            </div>                                
                        </div>
                    </div>                    
                </div>
                <div class="row mt-2 ms-3">
                    <ul class="nav nav-underline" id="myTab" role="tablist">
                        <div class="row w-100">
                            <?php 
                                foreach ($branch_name as $key => $row) {
                                    ?>
                                    <li class="nav-item col-lg-2" role="presentation">
                                        <button class="nav-link text-center <?php echo ($key == '0')?'active':''?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#branch<?php echo $row?>" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true"><?php echo $row?></button>
                                    </li>
                            <?php } 
                                ?>
                        </div>
                    </ul>
                    <hr class="tabs-line">
                </div>
                <div class="tab-content ms-3 mt-3" id="myTabContent">
                <?php 
                    foreach ($branch_value as $x => $branch) {
                        ?>
                        <div class="tab-pane fade <?php echo ($x == '0')?'show'.' '.'active':''?>" id="branch<?php echo $branch['branch_name']?>" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                            <div class="row">
                                <div class="col-lg-8">
                                <div class="row">
                                        <div class="col-12 col-lg-6">
                                            <table class="table table-borderless mb-0">
                                                <tr>
                                                    <td class="col-6 py-1">Contact Name</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_contact_name'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Country</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_country'] ?>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">State</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_state'] ?>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">City</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_city'] ?>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Status</td>
                                                    <td class="col-6 py-1 tbl-active">Active</td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-12 col-lg-6">
                                            <table class="table table-borderless">
                                                <tr>
                                                    <td class="col-6 py-1">GST</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_gst'] ?>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Email</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_email'] ?>
                                                </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Phone Number</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_contact_number'] ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-6 py-1">Contact Address</td>
                                                    <td class="col-6 py-1"><?php echo $branch['branch_contact_address'] ?>
                                                </td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>
                                </div> 
                                <div class="col-lg-4">
                                    <div class="row justify-content-center">
                                        <button class="main-btn col-5 col-lg-6"><?php echo $branch['branch_type']?></button>
                                    </div>                                
                                </div>
                            </div>
                            <hr class="tabs-line"> 
                            <h3 class="head-font ms-3 my-3">Banner Image</h3> 
                            <div class="row ms-2">
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $branch['branch_logo']?>" class="col-3">
                                <img src="<?php echo base_url(); ?>uploads/<?php echo $branch['branch_logo']?>" class="col-3">
                            </div>
                        </div>
                         
                    <?php }
                    ?>
                </div>
                <div class="row mt-3 ms-3">
                    <ul class="nav nav-underline" id="myTab22" role="tablist">
                        <li class="nav-item col-lg-3" role="presentation">
                            <button class="nav-link active" id="home-tab22" data-bs-toggle="tab" data-bs-target="#home-tab-pane22" type="button" role="tab" aria-controls="home-tab-pane22" aria-selected="true">Description</button>
                        </li>
                        <li class="nav-item col-lg-3" role="presentation">
                            <button class="nav-link" id="profile-tab22" data-bs-toggle="tab" data-bs-target="#profile-tab-pane22" type="button" role="tab" aria-controls="profile-tab-pane22" aria-selected="false">Ratings & Reviews</button>
                        </li>
                    </ul>
                    <hr class="tabs-line">
                    <div class="tab-content " id="myTabContent22">
                        <div class="tab-pane fade show active" id="home-tab-pane22" role="tabpanel" aria-labelledby="home-tab22" tabindex="0">
                            <div class="d-flex column-gap-2">
                                <div class="star-boder">
                                    <i class='fas fa-star'></i>
                                </div>
                                <div class="star-boder">
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                </div>
                                <div class="star-boder">
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                </div>
                                <div class="star-boder">
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                </div>
                                <div class="star-boder">
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                    <i class='fas fa-star'></i>
                                </div>
                            </div>
                            <div class="mt-5 w-50">
                                <span class="review-name">OP</span>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star-half'></i>
                                <i class='far fa-star'></i>
                                <i class='far fa-star'></i>
                                <div class="ms-5 mt-1">
                                    <p class="bold-font">Omar Sundaram . 9 hours ago<p>
                                    <p class="color-font">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                </div>    
                            </div>
                            <div class="mt-5 w-50">
                                <span class="review-name">OP</span>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star-half'></i>
                                <i class='far fa-star'></i>
                                <i class='far fa-star'></i>
                                <div class="ms-5 mt-1">
                                    <p class="bold-font">Omar Sundaram . 9 hours ago<p>
                                    <p class="color-font">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                            </div>    
                            </div>
                            <div class="mt-5 w-50">
                                <span class="review-name">OP</span>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star'></i>
                                <i class='fas fa-star-half'></i>
                                <i class='far fa-star'></i>
                                <i class='far fa-star'></i>
                                <div class="ms-5 mt-1">
                                    <p class="bold-font">Omar Sundaram . 9 hours ago<p>
                                    <p class="color-font">The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                                <div>    
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile-tab-pane22" role="tabpanel" aria-labelledby="profile-tab22" tabindex="0"></div>
                    </div>
                </div>             
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>