<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Merchant</title>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/add-merchant.css">
</head>
<body>
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>Add Merchant</h3>
                </div>
            </div>
            <div class="card">
                <div class="container">
                    <div class="row">
                        <div class="col">
                        <div class="step-load">
                                <p class="stp1-font" id="stp1-font"><span class="step1" id="step1">01</span>Personal Deatils</p>
                                <p class="load-form" id="load-form">- - - - - - - - - - - - -</p>
                                <p class="stp2-font" id="stp2-font"><span class="step2" id="step2">02</span>Branch</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <form action="<?php echo base_url();?>Add_merchant/add_merchant" enctype="multipart/form-data" id="form-add-merchant" method="post">
                                <div id="form-step1">
                                    <div class="container form-step1" >
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="restaurant_name">Restaurant Name</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_restaurant_name" id="restaurant_name"
                                                            placeholder="Restaurant Name">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="contact_name">Contact Name</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_contact_name" id="contact_name"
                                                            placeholder="Contact Name">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="merchantemail">Merchant Email</label>
                                                        <input type="email" class="form-control input-edit" name="merchant_email" id="merchantemail"
                                                            placeholder="Merchant Email">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="phone_number">Merchant Phone Number</label>
                                                        <input type="text" class="form-control input-edit numbersOnly"  name="merchant_phone_number" id="phone_number"
                                                            placeholder="Merchant Phone Number">
                                                    </div>                                                                                        
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="country">Country</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_country" id="country"
                                                            placeholder="Country">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="state">State</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_state" id="state"
                                                            placeholder="State">
                                                    </div>                                               
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="city">City</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_city" id="city"
                                                            placeholder="City">
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="website">Website</label>
                                                        <input type="text" class="form-control input-edit" name="merchant_website" id="website"
                                                            placeholder="Website">
                                                    </div>                                                                                            
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="restaurant_details">Restaurant Details</label>
                                                        <textarea class="form-control input-edit" id="restaurant_details" name="merchant_restaurant_details" rows="3" placeholder="Restaurant Details"></textarea>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <label class="form-label" for="contact_address">Contact Address</label>
                                                        <textarea class="form-control input-edit" id="contact_address" name="merchant_contact_address" rows="3" placeholder="Contact Address"></textarea>
                                                    </div>                                                                                        
                                                
                                                    </div>
                                                </div>
                                            <div class="col-lg-4">
                                                <div class="row">
                                                    <div class="col-11">
                                                        <label class="form-label" for="status">Status</label>
                                                        <select class="form-select input-edit" name="merchant_status" id="status" aria-label="Default select example">
                                                            <option disabled selected>Status</option>
                                                            <option value="active">Active</option>
                                                            <option value="inactive">Inactive</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-11">
                                                        <label for="image" class="form-label">Logo</label><br>
                                                        <!-- <input type="file" class="input-imagebox" id="input-image" value="Drag Your Image"> -->
                                                        <input class="form-control input-edit" name="merchant_logo" id="input-image" type="file">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                            
                                    </div>
                                    <div class="page-next">
                                        <div>
                                            <a href="<?php echo base_url();?>merchant">
                                                <button type="button" class="btn btn-cancel" name="cancel" id="cancel">Cancel</button>
                                            </a>
                                        </div>
                                        <div>
                                            <button type="button" class="btn btn-next" name="submit" id="submit">Next</button>        
                                        </div>
                                    </div>
                                </div>
                                <div class="d-none" id="form-step2">
                                    <div id="variant_form">
                                        <div class="container form-step2">
                                            <div class="row">
                                                <div class="col-lg-8">
                                                    <div class="row">
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="branch_name">Branch Name</label>
                                                            <input type="text" class="form-control input-edit" name="branch_name[]" id="branch_name"
                                                                placeholder="Branch Name">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="branch_type">Branch Type</label>
                                                            <select class="form-select input-edit" id="branch_type" name="branch_type[]" aria-label="Default select example">
                                                                <option disabled selected>Branch Type</option>
                                                                <option value="Main Branch">Main Branch</option>
                                                                <option value="Sub Branch">Sub Branch</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="contact_name1">Contact Name</label>
                                                            <input type="text" class="form-control input-edit" name="branch_contact_name[]" id="contact_name1"
                                                                placeholder="Contact Name">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="contact_number1">Contact Number</label>
                                                            <input type="text" class="form-control input-edit numbersOnly" name="branch_contact_number[]" id="contact_number1"
                                                                placeholder="Contact Number">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="email">Email</label>
                                                            <input type="email" class="form-control input-edit product-details" name="branch_email[]" id="email"
                                                                placeholder="Email">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="gst">GST</label>
                                                            <input type="text" class="form-control input-edit" name="branch_gst[]" id="gst"
                                                                placeholder="GST">
                                                        </div>

                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="country1">Country</label>
                                                            <input type="text" class="form-control input-edit" name="branch_country[]" id="country1"
                                                                placeholder="Country">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="city1">City</label>
                                                            <input type="text" class="form-control input-edit product-details" name="branch_city[]" id="city1"
                                                                placeholder="City">
                                                        </div>
                                                        <div class="col-lg-6">
                                                                <label class="form-label" for="state1">State</label>
                                                                <input type="text" class="form-control input-edit" name="branch_state[]" id="state1"
                                                                    placeholder="State">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="form-label" for="contact_address1">Contact Address</label>
                                                            <textarea class="form-control input-edit" id="contact_address1" name="branch_contact_address[]" rows="3" placeholder="Contact Address"></textarea>
                                                        </div>
                                                    </div>
                                                </div>            
                                                <div class="col-lg-4">
                                                    <div class="row">
                                                        <div class="col-11">
                                                            <label class="form-label" for="food_type">Food Type</label>
                                                            <select class="form-select input-edit" id="food_type" name="branch_food_type[]" aria-label="Default select example">
                                                                <option disabled selected>Food Type</option>
                                                                <option value="veg">Veg</option>
                                                                <option value="non veg">Non veg</option>
                                                                <option value="Vey & Non veg">Vey & Non veg</option>
                                                            </select>
                                                        </div>    
                                                        <div class="col-11">
                                                            <label class="form-label" for="category_type">Category Type</label>
                                                            <select class="form-select input-edit" id="category_type" name="branch_category_type[]" aria-label="Default select example">
                                                                <option disabled selected>Category Type</option>
                                                                <?php  
                                                                    foreach ($category as $row) { 
                                                                        ?>
                                                                        <option value="<?php echo $row->cuisine_name?>"><?php echo $row->cuisine_name?></option> 
                                                                <?php } 
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-11">
                                                            <label for="branch_image" class="form-label">Logo</label><br>
                                                            <!-- <input type="button" class="input-imagebox" id="input-image1" value="Drag Your Image"> -->
                                                            <input class="form-control input-edit" name="branch_logo[]" id="branch_image" type="file">
                                                        </div>
                                                        <div class="col-lg-11">
                                                            <label class="form-label" for="status">Status</label>
                                                            <select class="form-select input-edit" name="branch_status[]" id="status" aria-label="Default select example">
                                                                <option disabled selected>Status</option>
                                                                <option value="active">Active</option>
                                                                <option value="inactive">Inactive</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                       
                                        </div>
                                    </div>
                                    <div id="add_input" class=""></div>                                    
                                    <div class="page1-next row">
                                        <div class="col text-end">
                                            <button type="button" class="btn" name="cancel1" id="cancel1">Cancel</button>
                                            <button type="submit" class="btn btn-next" name="submit" id="submit">Save</button>
                                        </div>
                                        <div class="col text-end">
                                            <button type="button" class="btn add-variant" id="add_variant" name="add_variant">+ Add</button> 
                                            <button type="button" class="btn"  id="remove_variant" name="add_variant"> - Add</button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>                        
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-merchant").validate({
                rules: {
                    merchant_restaurant_name: {
                        required: true,
                    },
                    merchant_contact_name: {
                        required: true,
                    },
                    merchant_email: {
                        required: true,
                    },
                    merchant_phone_number: {
                        required: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    merchant_country: {
                        required: true,
                    },
                    merchant_state: {
                        required: true,
                    },
                    merchant_city: {
                        required: true,
                    },
                    merchant_website: {
                        required: true,
                    },
                    merchant_restaurant_details: {
                        required: true,
                    },
                    merchant_contact_address: {
                        required: true,
                    },
                    merchant_logo: {
                        required: true,
                    },
                    branch_name: {
                        required: true,
                    },
                    merchant_status: {
                        required: true,
                    },
                    branch_type: {
                        required: true,
                    },
                    branch_food_type: {
                        required: true,
                    },
                    branch_contact_name: {
                        required: true,
                    },
                    branch_contact_number: {
                        required: true,
                        minlength: 10,
                        maxlength: 10,
                    },
                    category_type: {
                         required: true,
                    },
                    branch_email: {
                        required: true,
                    },
                    branch_gst: {
                        required: true,
                    },
                    branch_country: {
                        required: true,
                    },
                    branch_state: {
                        required: true,
                    },
                    branch_city: {
                        required: true,
                    },
                    branch_contact_address: {
                        required: true,
                    },
                    branch_logo: {
                        required: true,
                    },
                    branch_status: {
                        required: true,
                    },
                
                },
                messages: {
                    merchant_restaurant_name: {
                        required: "Enter Restaurant Name",
                    },
                    merchant_contact_name: {
                        required: "Enter Contact Name",
                    },
                    merchant_email: {
                        required: "Enter Email",
                    },
                    merchant_phone_number: {
                        required: "Enter Phone Number",
                    },
                    merchant_country: {
                        required: "Enter Your Country",
                    },
                    merchant_state: {
                        required: "Enter Your State",
                    },
                    merchant_city: {
                        required: "Enter Your City",
                    },
                    merchant_website: {
                        required: "Enter Your Website",
                    },
                    merchant_restaurant_details: {
                        required: "Enter Restaurant Details",
                    },
                    merchant_contact_address: {
                        required: "Enter Contct Address",
                    },
                    merchant_logo: {
                        required: "Select the image",
                    },
                    merchant_status: {
                        required: "Select the Status",
                    },
                    branch_name: {
                        required: "Enter Branch Name",
                    },
                    branch_type: {
                        min: "Select Branch Type",
                    },
                    branch_food_type: {
                        min: "Select Food Type",
                    },
                    branch_contact_name: {
                        required: "Enter Contact Name",
                    },
                    branch_contact_number: {
                        required: "Enter Contact Number",
                    },
                    category_type: {
                        min: "Select Category Type",
                    },
                    branch_email: {
                        required: "Enter Email",
                    },
                    branch_gst: {
                        required: "Enter GST",
                    },
                    branch_country: {
                        required: "Enter Country",
                    },
                    branch_state: {
                        required: "Enter Your State",
                    },
                    branch_city: {
                        required: "Enter Your City",
                    },
                    branch_contact_address: {
                        required: "Enter Your Contact Address",
                    },
                    branch_logo: {
                        required: "Select the image",
                    },
                    branch_status: {
                        required: "Enter Your Status",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            }); 

            let nextForm = document.getElementById("submit");
            nextForm.addEventListener("click", forwardForm);

            function forwardForm() {
                if ($("#form-add-merchant").valid() == true) {                    
                    form1.classList.add("d-none");
                    form2.classList.remove("d-none");
                    step1.classList.add("remove-stp1");
                    step2.classList.add("add-stp2")
                    loadLine.classList.add("remove-load");
                    no1.classList.add("remove-no1");
                    no2.classList.add("remove-no2");
                }
            }

            let prevForm = document.getElementById("cancel1");
            prevForm.addEventListener("click", previousForm);

            function previousForm() {
                form1.classList.remove("d-none");
                form2.classList.add("d-none");
                step1.classList.remove("remove-stp1");
                step2.classList.remove("add-stp2")
                loadLine.classList.remove("remove-load");
                no1.classList.remove("remove-no1");
                no2.classList.remove("remove-no2");
                if ($("#form-add-merchant").valid() == true) {
                    $("#form-add-merchant").submit();
                }
            }
        });
        
        let form1 = document.getElementById("form-step1");
        let form2 = document.getElementById("form-step2");
        
        let step1 = document.getElementById("stp1-font");
        let step2 = document.getElementById("stp2-font");
        let loadLine = document.getElementById("load-form");
        let no1 = document.getElementById("step1");
        let no2 = document.getElementById("step2");

        let control = document.getElementById("variant_form").innerHTML;
        let addinput = document.getElementById("add_input");
        // console.log(control);
        document.getElementById("add_variant").addEventListener("click", addfunction);
        function addfunction() {
            if (document.getElementById('add_input').classList.contains("d-none")) {
                addinput.classList.remove("d-none");
            } else {
                document.getElementById("add_input").innerHTML += control;
            }
                        
        }
        document.getElementById("remove_variant").addEventListener("click", removefunction);
        function removefunction() {
            addinput.classList.add("d-none");
        }
    </script>   
</body>
</html>