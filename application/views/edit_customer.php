<body>
    <?php 
        // echo "<pre>";
        // print_r($customer_edit);
        // exit;
    ?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/customer.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div>
                <div class="modal-dialog model-items">
                    <div class="modal-content bg-white model-box">
                        <div class="modal-body p-4">
                            <div id="page-1">
                                <h3 class="text-center">Edit Customer</h3>
                                <form action="<?php echo base_url();?>Customer/customer_update" class="row" id="customer-form" enctype="multipart/form-data" method="post">
                                    <div class="col-lg-4">
                                        <label class="form-label d-none" for="firstname">First Name</label>
                                        <input type="hidden" id="custId" name="customer_id" value="<?php echo $customer_edit[0]['customer_id']?>">
                                        <input type="text" class="form-control input-edit-form" name="customer_firstname" id="firstname"
                                            placeholder="First Name" value ="<?php echo $customer_edit[0]['customer_firstname']?>">
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="form-label d-none" for="lastname">Last Name</label>
                                        <input type="text" class="form-control input-edit-form" name="customer_lastname" id="lastname"
                                            placeholder="Last Name" value ="<?php echo $customer_edit[0]['customer_lastname']?>">
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="form-label d-none" for="contactno">Contact No</label>
                                        <input type="text" class="form-control input-edit-form numbersOnly" name="customer_no" id="Contact No"
                                            placeholder="Contact No" value ="<?php echo $customer_edit[0]['customer_no']?>">
                                        </select>
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="form-label d-none" for="email">Email</label>
                                        <input type="text" class="form-control input-edit-form" name="customer_email" id="email"
                                            placeholder="Email" value ="<?php echo $customer_edit[0]['customer_email']?>">
                                    </div>
                                    <div class="col-lg-4">
                                        <label class="form-label d-none" for="status">Status</label>
                                        <select class="form-select input-edit-form" name="customer_status" id="status" aria-label="Default select example">
                                            <option selected><?php echo $customer_edit[0]['customer_status']?></option>
                                            <option value="active">Active</option>
                                            <option value="inactive">Inactive</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-5">
                                        <label for="image1" class="form-label d-none">Select Your Image</label>
                                        <input class="form-control input-edit-form" name="customer_image" id="image1" type="file">
                                        <P><?php echo $customer_edit[0]['customer_image']?></p>
                                        <img class="w-25" src="<?php echo base_url();?>uploads/<?php echo $customer_edit[0]['customer_image']?>">
                                    </div>
                                    <div class="row justify-content-center ms-2 my-4">
                                        <div class="col-3">
                                            <button type="submit" class="btn add-btn mb-2" id="submit">Submit</button>
                                            <button type="button" class="btn cancel-btn mb-2">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#customer-form").validate({
                rules: {
                    customer_firstname: {
                        required: true,
                    },
                    customer_lastname: {
                        required: true,
                    },
                    customer_no: {
                        required: true,
                    },
                    customer_email: {
                        required: true,
                    },
                    customer_status: {
                        required: true,
                    },                    
                },
                messages: {
                    customer_firstname: {
                        required: "Enter Your First Name",
                    },
                    customer_lastname: {
                        required: "Enter Your Last Name",
                    },
                    customer_contactno: {
                        required: "enter Contact No",
                    },
                    customer_email: {
                        required: "Enter Email",
                    },
                    customer_status: {
                        required: "Select Status",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });
            let page1 =document.getElementById("page-1");
            let page2 =document.getElementById("page-2");          
            

            let submit =document.getElementById("submit");
            submit.addEventListener("click",hide1);

            function hide1() {
                if ($("#customer-form").valid() == true) {
                    page1.classList.add("d-none");                    
                    page2.classList.remove("d-none");
                }
            }
            let cancel =document.getElementById("cancel");
            cancel.addEventListener("click", show1);

            function show1() {
                page1.classList.remove("d-none");                    
                page2.classList.add("d-none");
            }
        });

</script>
</body>
