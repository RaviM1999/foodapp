<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sub-category.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="customise-modal">
            <div class="modal-dialog model-items">
                <div class="modal-content bg-white model-box">
                    <div class="modal-body p-4">
                        <h1 id="tle-1">Edit Sub Category</h1>
                        <form action="<?php echo base_url();?>Subcategory/update_sub_category" enctype="multipart/form-data" id="form-add-sub-category" method="post">
                            <div class="modal-form-edit row" id="modal-form-edit">
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="merchant">Merchant Name</label>
                                    <input type="hidden" id="custId" name="sub_category_id" value="<?php echo $sub_category[0]['sub_category_id']?>">
                                    <select class="form-select" name="sub_category_name" id="sub_category_namet" aria-label="Default select example">
                                        <option selected><?php echo $sub_category[0]['sub_category_name']?></option>
                                        <option value="kfc">KFC</option>
                                        <<?php 
                                            foreach ($category_data as $category) {
                                            ?>
                                            <option value="<?php echo $category->category_name?>"><?php echo $category->category_name?></option> 
                                        <?php } 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="branch">Branch Name</label>
                                    <select class="form-select" name="sub_category_branch" id="sub_category_branch" aria-label="Default select example">
                                        <option selected><?php echo $sub_category[0]['sub_category_branch']?></option>
                                        <<?php 
                                            foreach ($category_data as $category) {
                                            ?>
                                            <option value="<?php echo $category->category_branch?>"><?php echo $category->category_branch?></option> 
                                        <?php } 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <label class="form-label d-none" for="category">Category</label>
                                    <select class="form-select" name="sub_category" id="sub_category" aria-label="Default select example">
                                        <option selected><?php echo $sub_category[0]['sub_category']?></option>
                                        <?php 
                                            foreach ($category_data as $category) {
                                            ?>
                                            <option value="<?php echo $category->category_category?>"><?php echo $category->category_category?></option> 
                                        <?php } 
                                        ?>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="subcategory">Sub Category</label>
                                    <input type="text" class="form-control" name="sub_category_list" id="subcategory"
                                        placeholder="Sub Category Name" value="<?php echo $sub_category[0]['sub_category_list']?>">
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="status">Status</label>
                                    <select class="form-select" name="sub_category_status" id="status" aria-label="Default select example">
                                        <option selected><?php echo $sub_category[0]['sub_category_status']?></option>
                                        <option value="active">Active</option>
                                        <option value="inactive">Inactive</option>
                                    </select>
                                </div>
                                <div class="col-lg-4">
                                    <label class="form-label d-none" for="description">Description</label>
                                    <textarea class="form-control" name="sub_category_description" id="description" rows="3" placeholder="Description"><?php echo $sub_category[0]['sub_category_description']?></textarea>
                                </div>
                                <div class="col-lg-4">
                                    <label for="image" class="form-label d-none">Select Your Image</label>
                                    <input class="form-control" name="sub_category_image" id="image" type="file">
                                    <p><?php echo $sub_category[0]['sub_category_image']?></p>
                                    <img class="w-25" src="<?php echo base_url();?>uploads/<?php echo $sub_category[0]['sub_category_image']?>">
                                </div>
                            </div>
                            <div class="modal-submit" id="modal-submit">
                                <div>
                                    <a href="<?php echo base_url()?>sub_category">
                                        <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                    </a>
                                </div>
                                <div>
                                    <button type="submit" class="btn" name="submit" id="submit1">Save</button>        
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>
        $(document).ready(function(){
            $("#form-add-sub-category").validate({
                rules: {
                    sub_category_name: {
                        required: true,
                    },
                    sub_category_branch: {
                        required: true,
                    },
                    sub_category: {
                        required: true,
                    },
                    sub_category_list: {
                        required: true,
                    },
                    sub_category_status: {
                        required: true,
                    },
                    sub_category_description: {
                        required: true,
                    },
                    
                },
                messages: {
                    sub_category_name: {
                        min: "Select Your Merchant",
                    },
                    sub_category_branch: {
                        min: "Select Your Branch",
                    },
                    sub_category: {
                        min: "Select Your Category",
                    },
                    sub_category_list: {
                        required: "Enter Sub Category",
                    },
                    sub_category_status: {
                        min: "Select Status",
                    },
                    sub_category_description: {
                        required: "Enter Description",
                    },
                }
            });
        });
    </script>
</body>