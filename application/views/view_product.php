<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/add-merchant-item.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3>View Product Details</h3>
                </div>
            </div>
                <div class="preview-products" id="preview-products">
                    <div class="container">
                        <div class="row ms-1">
                            <div class="col-lg-3">
                                <img src="<?php echo base_url();?>uploads/<?php echo $productlist[0]["product_image"]?>" class="img-fluid">
                            </div>
                            <div class="col">
                                <div class="row">
                                    <h3><?php echo $productlist[0]["product_category"]?></h3>
                                </div>
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless mb-0">
                                            <tr>
                                                <td class="py-1">Price</td>
                                                <td class="py-1"><?php echo $productlist[0]["product_price"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Merchant Name</td>
                                                <td class="py-1"><?php echo $productlist[0]["product_name"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Branch</td>
                                                <td class="py-1"><?php echo $productlist[0]["product_branch"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Category</td>
                                                <td class="py-1"><?php echo $productlist[0]["product_category"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="py-1">Sub-Category</td>
                                                <td class="py-1"><?php echo $productlist[0]["product_sub_category"]?></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <table class="table table-borderless">
                                            <tr>
                                                <td class="col-6 py-1">Description</td>
                                                <td class="col-6 py-1"><?php echo $productlist[0]["product_description"]?></td>
                                            </tr>
                                            <tr>
                                                <td class="col-6 py-1">Status</td>
                                                <td class="col-6 py-1"><?php echo $productlist[0]["product_status"]?></td>
                                            </tr>
                                        </table>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                        <div class="row m-3">
                            <ul class="nav nav-underline" id="myTab" role="tablist">
                                <?php  
                                    foreach ($product_var_variant as $key => $productvariant) { 
                                        ?>  <li class="nav-item col-12 col-sm-3 col-lg-2" role="presentation">
                                                <button class="nav-link <?php echo ($key == '0')?'active':''?>" id="home-tab" data-bs-toggle="tab" data-bs-target="#<?php echo $productvariant?>" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true"><?php echo $productvariant?></button>
                                            </li>  
                                    <?php }
                                ?>
                            </ul>                            
                            <hr class="tabs-line">
                            <div class="tab-content" id="myTabContent">
                                <?php
                                $i = 1;
                                foreach ($variantproduct as $x => $value) { 
                                    ?> 
                                        <div class="tab-pane fade <?php echo ($x == '0')?'show'.' '.'active':''?>" id="<?php echo $value['product_var_variant']?>" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                            <div class="row mt-4">
                                                <div class="col-12 col-lg-5">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <td class="col-6 py-1">Price</td>
                                                            <td class="py-1"><?php echo $value['product_var_price']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-6 py-1">Description</td>
                                                            <td class="py-1"><?php echo $value['product_var_description']?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-6 py-1">Created Date</td>
                                                            <td class="col-6 py-1">22/04/23</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="col-6 py-1">Status</td>
                                                            <td class="col-6 py-1"><?php echo $value['product_var_status']?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-3 col-lg-3">
                                                    <div id="carouselExample<?php echo $i;?>" class="carousel slide">
                                                        <div class="carousel-inner">
                                                            <div class="carousel-item active">
                                                                <img src="<?php echo base_url();?>uploads/<?php echo $value['product_var_image']?>" class="d-block w-100" alt="...">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img src="<?php echo base_url();?>uploads/<?php echo $value['product_var_image']?>" class="d-block w-100" alt="...">
                                                            </div>
                                                            <div class="carousel-item">
                                                                <img src="<?php echo base_url();?>uploads/<?php echo $value['product_var_image']?>" class="d-block w-100" alt="...">
                                                            </div>
                                                        </div>
                                                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExample<?php echo $i;?>" data-bs-slide="prev">
                                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                            <span class="visually-hidden">Previous</span>
                                                        </button>
                                                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExample<?php echo $i;?>" data-bs-slide="next">
                                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                            <span class="visually-hidden">Next</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             <?php $i++; } 
                                    ?>
                            </div>
                        </div>
                        <div class="final-view" id="final-view">
                            <div>
                                <button type="button" class="btn d-none" name="cancel" id="cancel2">Back</button>
                            </div>
                            <div>
                                <a href="<?php echo base_url();?>merchant_items">
                                <button type="button" class="btn submit3" name="submit3" id="submit3">Save</button></a>        
                            </div>
                        </div> 
                    </div>
                </div>               
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
</body>
