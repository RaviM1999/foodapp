<body>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin-merchant-category.css">
    <?php include 'sidebar-navbar.php';?>
    <div class="customise-modal">
        <div class="modal-dialog model-items">
            <div class="modal-content bg-white model-box">
                <div class="modal-body">
                    <h1 class="title-1" id="title-1">Edit Category</h1>
                    <form action="<?php echo base_url()?>Merchantcategory/update_category" enctype="multipart/form-data" id="form-add-merchant-category" method="post">
                        <div class="modal-form-edit row justify-content-between" id="modal-form-edit">
                            <div class="col-lg-4 p-0">
                                <label class="form-label d-none" for="merchant">Product Name</label>
                                <input type="hidden" id="custId" name="category_id" value="<?php echo  $category_value[0]['category_id']?>">
                                <select class="form-select input-edit" name="category_name" id="merchant" aria-label="Default select example">
                                    <option selected><?php echo $category_value[0]['category_name']?></option>
                                    <?php foreach ($merchant_data as $data) { ?>
                                        <option data-price="<?php echo $data['merchant_id']?>" value="<?php echo $data['merchant_restaurant_name']?>"><?php echo $data['merchant_restaurant_name']?></option> 
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-lg-4 p-0">
                                <label class="form-label d-none" for="branch">Branch</label>
                                <select class="form-select input-edit" name="category_branch" id="category_branch" aria-label="Default select example">
                                    <option selected><?php echo $category_value[0]['category_branch']?></option>
                                    <?php foreach ($branch_value as $branch_name) { ?>
                                            <option value="<?php echo $branch_name?>"><?php echo $branch_name?></option> 
                                        <?php } ?>
                                </select>
                            </div>
                            <div class="form-check check-bow-edit col-lg-3 p-0">
                                <input class="form-check-input" type="checkbox" name="offer_checkbox" id="flexCheckChecked" checked>
                                <label class="form-check-label" for="flexCheckChecked">Is offer enabled</label>
                            </div>
                            <div class="col-lg-4 p-0">
                                <label class="form-label d-none" for="category">Category</label>
                                <input type="text" class="form-control input-edit" name="category_category" id="category"
                                    placeholder="Category Name" value="<?php echo $category_value[0]['category_category']?>">
                            </div>
                            <div class="col-lg-4 p-0">
                                <label class="form-label d-none" for="offerpercentage">Offer Percentage</label>
                                <input type="text" class="form-control input-edit numbersOnly" name="category_offer_percentage" id="offerpercentage"
                                    placeholder="Offer Percentage"  value="<?php echo $category_value[0]['category_offer_percentage']?>">
                            </div>
                            <div class="col-lg-3 p-0">
                                <label class="form-label d-none" for="status">Status</label>
                                <select class="form-select input-edit" name="category_status" id="status" aria-label="Default select example">
                                    <option selected><?php echo $category_value[0]['category_status']?></option>
                                    <option value="active">Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>
                            <div class="col-12 p-0">
                                <label class="form-label d-none" for="description">Description</label>
                                <textarea class="form-control input-edit" name="category_description" id="description" rows="3" placeholder="Description"><?php echo $category_value[0]['category_description']?></textarea>
                            </div>
                            <div class="col-lg-4">
                                <label for="image" class="form-label d-none">Select Your Image</label>
                                <input class="form-control input-edit" name="category_image" id="image" type="file">
                                <p><?php echo $category_value[0]['category_image']?></p>
                                <img class="w-25" src="<?php echo base_url();?>uploads/<?php echo $category_value[0]['category_image']?>">
                            </div>
                        </div>
                        <div class="modal-submit" id="modal-submit"> 
                            <div>
                                <a href="<?php echo base_url();?>category_merchant">
                                    <button type="button" class="btn" name="cancel" id="cancel">Cancel</button>
                                </a>
                            </div>
                            <div>
                                <button type="submit" class="btn" name="submit" id="submit1">Save</button>        
                            </div>
                        </div> 
                    </form>                        
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.5/dist/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
    <script>

        $(document).ready(function(){
            $("#form-add-merchant-category").validate({
                rules: {
                    category_name: {
                        required: true,
                    },
                    category_branch: {
                        required: true,
                    },
                    category_category: {
                        required: true,
                    },
                    category_offer_percentage: {
                        required: true,
                    },
                    category_status: {
                        required: true,
                    },
                    category_description: {
                        required: true,
                    },
                    // category_image: {
                    //     required: true,
                    // },
                    
                },
                messages: {
                    category_name: {
                        min: "Select Your Merchant",
                    },
                    category_branch: {
                        required: "Enter Your Branch",
                    },
                    category_category: {
                        required: "Enter Category",
                    },
                    category_offer_percentage: {
                        required: "Enter Offer Percentage",
                    },
                    category_status: {
                        min: "Select Status",
                    },
                    category_description: {
                        required: "Enter Description",
                    },
                    category_image: {
                        required: "Upload Image",
                    },
                }
            });
            jQuery('.numbersOnly').keyup(function () { 
                this.value = this.value.replace(/[^0-9\.]/g,'');
            });
            
        });
    title1 = document.getElementById("title-1");
    totalView = document.getElementById("total-view");
    modalSubmit = document.getElementById("modal-submit");    
    modalForm = document.getElementById("modal-form-edit");   
    reviewItem = document.getElementById("review-item");
    </script>
</body>
