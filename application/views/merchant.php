<body>
    <?php 
        // echo "<pre>";
        // print_r($merchant_data) ;
        // exit;
    ?>
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/admin-merchant .css">
    <?php include 'sidebar-navbar.php';?>
    <div class="main-contents" id="main-contents">
        <div class="container">
            <div class="row">
                <div class="col">
                    <p class="home-text d-none">Home <span> / Merchant</span></p>
                    <h3>Merchant<h3>
                </div>
            </div>
            <div class="card">
                <div class="row p-4">
                    <form class="tittle-box-form2 col-lg-10" role="search">
                        <div class="row">
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="Search" aria-label="Search">                                             
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="From Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <input class="form-control input-edit" type="search" placeholder="To Date" aria-label="Search">                                            
                            </div>
                            <div class="col-6 col-lg-2">
                                <select class="form-select input-edit" id="status" name="status" aria-label="Default select example">
                                    <option selected>Status</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>                                            
                            </div>
                            <div class="col-lg-2">
                                <button type="button" class="btn p-0" name="cancel" id="cancel123"><i class="material-icons check-icon">&#xe5d5;</i></button>
                                <button type="button" class="btn p-0" name="cancel" id="cancel167"><i class="material-icons check-icon">&#xe8b6;</i></button> 
                            </div>
                        </div>
                    </form>
                    <div class="col-lg-2">
                            <a href="<?php echo base_url();?>add_merchant" class="btn add-btn">Add</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="table-responsive">
                            <table class="table table-borderless text-left">
                                <thead>
                                    <tr>
                                        <th>Merchant ID</th>
                                        <th>Company Name</th>
                                        <th>Founder Name</th>
                                        <th>Contact No</th>
                                        <th>Email-Id</th>
                                        <th class="text-center">No.of.Branches</th>
                                        <th class="text-center">Date</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $i=1;
                                        foreach ($merchant_data as $merchant) {
                                            ?>
                                            <tr>
                                                <td>#MR0<?php echo $i?></td>
                                                <td><img class ="product-image" src="<?php echo base_url();?>uploads/<?php echo $merchant['merchant_logo']?>"><?php echo $merchant['merchant_restaurant_name']?></td>
                                                <td><?php echo $merchant['merchant_contact_name']?></td>
                                                <td><?php echo $merchant['merchant_phone_number']?></td>
                                                <td><?php echo $merchant['merchant_email']?></td>
                                                <td class="text-center">5</td>    
                                                <td class="text-center text-nowrap "><?php echo $merchant['date']?></td>
                                                <td class="text-center <?php echo ($merchant['merchant_status'] == "active")?'tbl-active':'' ?>"><?php echo $merchant['merchant_status']?></td>
                                                <td class="text-center">
                                                <div class="dropdown">
                                                    <a class="dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                                        <button>...</button>
                                                    </a>
                                                    <ul class="dropdown-menu collapse">
                                                        <li><a class="dropdown-item" href="<?php echo base_url();?>Merchant/view_merchant?merchant_id=<?php echo $merchant['merchant_id']?>"><i class="material-icons text-center">&#xe417;</i>View</a></li>
                                                        <li><a class="dropdown-item" href="<?php echo base_url();?>Merchant/delete_merchant?<?php echo $merchant['merchant_id']?>"><i class="material-icons text-center">&#xe254;</i>delete</a></li>
                                                        <li><a class="dropdown-item" href="<?php echo base_url();?>Merchant/edit_merchant?merchant_id=<?php echo $merchant['merchant_id']?>"><i class="material-icons text-center">&#xe872;</i>Edit</a></li>
                                                    </ul>
                                                </div>
                                                </td>                                           
                                            </tr> 
                                    <?php $i++; }
                                        ?>
                                     
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-end mx-4">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Previous">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#" aria-label="Next">
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custommain.js"></script>
</body>
