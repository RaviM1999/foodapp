<?php 
class Variants_con extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Variant_model');
    }
    public function variants_add() {
        if($this->input->post()) {
            $variant_data = $this->input->post();
            unset($variant_data['submit']);
            $this->Variant_model->variant_add($variant_data);
        }
        $variant_data['variants'] = $this->Variant_model->variant_data();
        $this->load->view('variant', $variant_data);
    }
    public function variant_delete() {
        $variant_ids = $this->input->server('QUERY_STRING');
        $var_data = $this->Variant_model->variant_delete($variant_ids);
        if($var_data == TRUE) {
            redirect ('variants');
        }
    }
    public function variant_edit() {
       $edit_var_id = $this->input->server('QUERY_STRING');
       $variant_data['variant_value'] = $this->Variant_model->edit_variant($edit_var_id);
        $this->load->view('edit-variant', $variant_data);
    }
    public function variant_update() {
        $variant_datas = $this->input->post();
        unset($variant_datas['submit']);
        $this->Variant_model->update_variant($variant_datas);
        redirect ('variants');
    }
}