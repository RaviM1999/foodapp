<?php 
class Merchantcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->library('upload');
        $this->load->model('Merchantcategory_model');
        $this->load->model('Merchant_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function category_data($id) {
        // ajax call
        $merchant_id_v =  $this->Merchant_model->get_merchant($id);
        $merchant_branch['merchant_branch'] = $merchant_id_v[0]['branch_name'];

        print_r($merchant_branch['merchant_branch']);
    }
    public function category_merchant() {
        if ($this->input->post()) {
            $category_data = $this->input->post();
            unset($category_data['submit']);
            $category_data['category_image'] = date("Ymdhi").$_FILES['category_image']['name'];
            $this->Merchantcategory_model->category($category_data);

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']        = date("Ymdhi").$_FILES['category_image']['name'];                   
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('category_image')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
        }
        $category_result['category_list'] = $this->Merchantcategory_model->category_data();

        // <---merchant data take to for category module--->
        $merchant_data = $this->Merchant_model->merchant();
        $category_result['merchant_data'] = $merchant_data;
        // <---merchant data take to for category module-->

        $this->load->view('merchant-category', $category_result);
    }
    public function delete_data() {
        $categoreyid = $this->input->server('QUERY_STRING');
        $resul = $this->Merchantcategory_model->delete_category($categoreyid);
        if ($resul == true) {
            redirect ('category_merchant');
        }
    }
    public function edit_category() {
        $edit_datas = $this->input->server('QUERY_STRING');
        $edit_categorys['category_value'] = $this->Merchantcategory_model->edit_category($edit_datas);
    
       // <---merchant data take to for category model--->
       $edit_categorys['branch_value'] = json_decode($edit_categorys['category_value'][0]['branch_name']);

       $merchant_data = $this->Merchant_model->merchant();
        
       $edit_categorys['merchant_data'] = $merchant_data;
        // <---merchant data take to for category model-->
        $this->load->view('edit_category', $edit_categorys);    
    }
    public function update_category() {
        $update_list = $this->input->post();
        unset($update_list['submit']);
        if (!empty($_FILES['category_image']['name'])) {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']        = date("Ymdhi").$_FILES['category_image']['name'];                    
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
                if ($this->upload->do_upload('category_image')) {
                    echo "success---1";
                } else {
                    echo "Fail";
                }
            $update_list['category_image'] = date("Ymdhi").$_FILES['category_image']['name']; 
        } 
       $this->Merchantcategory_model->update_data($update_list);
       redirect ('category_merchant');
    }
}