<?php 
class Merchantitem extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
         $this->load->library('upload');
        $this->load->model('Merchantitem_model');
    }    
    public function merchant_item() {
        $merchantlist['productlist'] = $this->Merchantitem_model->productdata();
        $this->load->view('merchant-item', $merchantlist);
    }
    public function view_items() {
        $productid = $this->input->server('QUERY_STRING');
        $merchantlist['productlist'] = $this->Merchantitem_model->viewdatas($productid);
        $merchantlist['product_var_variant']  = json_decode($merchantlist['productlist'][0]['product_var_variant']);
        $listproduct =array( 
            'product_var_variant' => $merchantlist['product_var_variant'],
            'product_var_price' => json_decode($merchantlist['productlist'][0]['product_var_price']),
            'product_var_description' => json_decode($merchantlist['productlist'][0]['product_var_description']),
            'product_var_status' => json_decode($merchantlist['productlist'][0]['product_var_status']),
            'product_var_image' => json_decode($merchantlist['productlist'][0]['product_var_image'])
        );
        foreach ($listproduct as $key => $valueArr) {
            if (is_array($valueArr)) { // file input "multiple"
                foreach($valueArr as $i=>$value) {
                    $filesByInput[$i][$key] = $value;
                }
            }
        }
        $files2 = $filesByInput;
       foreach ($files2 as $dom) {
            $variantdetails[] = $dom;
       }
       $merchantlist['variantproduct'] = $variantdetails;
        $this->load->view('view_product', $merchantlist);
    }
    public function delete_product() {
        $productid_item = $this->input->server('QUERY_STRING');
        $delete_result = $this->Merchantitem_model->deletedata($productid_item);
        if ($delete_result == true) {
            redirect ('merchant_items');
        }   
    }
    public function edit_product() {
        $product_edit = $this->input->server('QUERY_STRING');
       
        $editmerchant['editproduct'] = $this->Merchantitem_model->editdata($product_edit);
        $edit_list = array (
            'product_var_variant' =>json_decode($editmerchant['editproduct'][0]['product_var_variant']),
            'product_var_price' => json_decode($editmerchant['editproduct'][0]['product_var_price']),
            'product_var_description' => json_decode($editmerchant['editproduct'][0]['product_var_description']),
            'product_var_status' => json_decode($editmerchant['editproduct'][0]['product_var_status']),
            'product_var_image' => json_decode($editmerchant['editproduct'][0]['product_var_image'])
        );
        foreach ($edit_list as $keys => $values) {
            if (is_array($values)) { // file input "multiple"
                foreach($values as $i=>$data) {
                    $filesByInput[$i][$keys] = $data;
                }
            }
        }
        $filedata = $filesByInput;
        foreach ($filedata as $int) {
            $variantdetails[] = $int;
        }
        $editmerchant['variantlist'] = $variantdetails;
        $this->load->view('edit_merchant_item', $editmerchant);
    }  
    
    public function update_products() {
        if (!empty($_FILES['product_image']['name'])) {
            foreach ($_FILES as $x => $file) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                if ($x == 'product_image') {
                    $config['file_name']        = date("Ymdhi").$file['name'];
                }                    
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if($x == 'product_image') {
                        if ($this->upload->do_upload('product_image')) {
                            // echo "success---1";
                        } else {
                            echo "Fail";
                        }
                    } 
            }
            $update_data['product_image'] = date("Ymdhi").$_FILES['product_image']['name'];
        } 
        
        if ($_FILES['product_var_image']['name'] != "") {
            $filesname = $_FILES['product_var_image'];
            foreach ($filesname['name'] as $in => $image) {
                $_FILES['product_var_image[]']['name'] = $filesname['name'][$in];
                $_FILES['product_var_image[]']['type'] = $filesname['type'][$in];
                $_FILES['product_var_image[]']['tmp_name'] = $filesname['tmp_name'][$in];
                $_FILES['product_var_image[]']['size'] = $filesname['size'][$in];

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = date("Ymdhi").$image;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('product_var_image[]')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
                    $variant_name[] =  $config['file_name'];    
            }
            $update_data['product_var_image'] = $variant_name;
            $update_data['product_var_image']  = json_encode($update_data['product_var_image']);
        } 

        $update_data = $this->input->post();
        
        unset($update_data['submit2']);
        $update_data['product_var_variant']  = json_encode($update_data['product_var_variant']);
        $update_data['product_var_price']  = json_encode($update_data['product_var_price']);
        $update_data['product_var_description']  = json_encode($update_data['product_var_description']);
        $update_data['product_var_status']  = json_encode($update_data['product_var_status']);
        $this->Merchantitem_model->updatedata($update_data);
        
        redirect ('merchant_items');
    } 
}