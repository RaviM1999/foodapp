<?php 
class Add_merchant extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Merchant_model');
    }
    public function add_merchant() {
        if($this->input->post()) {
           $merchant_data = $this->input->post();
           unset($merchant_data['submit']);
           foreach ($_FILES as $x => $file) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                if ($x == 'merchant_logo') {
                    $config['file_name']        = date("Ymdhi").$file['name'];
                }                    
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if($x == 'merchant_logo') {
                        if ($this->upload->do_upload('merchant_logo')) {
                            // echo "success---1";
                        } else {
                            echo "Fail";
                        }
                    } 
            }
            $filesname = $_FILES['branch_logo'];
            foreach ($filesname['name'] as $in => $image) {
                $_FILES['branch_logo[]']['name'] = $filesname['name'][$in];
                $_FILES['branch_logo[]']['type'] = $filesname['type'][$in];
                $_FILES['branch_logo[]']['tmp_name'] = $filesname['tmp_name'][$in];
                $_FILES['branch_logo[]']['size'] = $filesname['size'][$in];

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = date("Ymdhi").$image;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('branch_logo[]')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
                
                $imagename[] =  $config['file_name'];
            }
            $merchant_data['merchant_logo']  = date("Ymdhi").$_FILES['merchant_logo']['name'];
            $merchant_data['branch_logo'] = $imagename;
            $merchant_data['branch_name']  = json_encode($merchant_data['branch_name']);
            $merchant_data['branch_type']  = json_encode($merchant_data['branch_type']);
            $merchant_data['branch_contact_name']  = json_encode($merchant_data['branch_contact_name']);
            $merchant_data['branch_contact_number']  = json_encode($merchant_data['branch_contact_number']);
            $merchant_data['branch_email']  = json_encode($merchant_data['branch_email']);
            $merchant_data['branch_gst']  = json_encode($merchant_data['branch_gst']);
            $merchant_data['branch_country']  = json_encode($merchant_data['branch_country']);
            $merchant_data['branch_city']  = json_encode($merchant_data['branch_city']);
            $merchant_data['branch_state']  = json_encode($merchant_data['branch_state']);
            $merchant_data['branch_contact_address']  = json_encode($merchant_data['branch_contact_address']);
            $merchant_data['branch_status']  = json_encode($merchant_data['branch_status']);
            $merchant_data['branch_food_type']  = json_encode($merchant_data['branch_food_type']);
            $merchant_data['branch_category_type']  = json_encode($merchant_data['branch_category_type']);
            $merchant_data['branch_logo']  = json_encode($merchant_data['branch_logo']);
            $this->Merchant_model->add_merchant($merchant_data);
            redirect ('merchant');
        }
        $merchant_cat['category'] = $this->Merchant_model->merchant_category();
        // echo "<pre>";
        // print_r($merchant_cat);
        // exit;
        
        $this->load->view('add-merchant', $merchant_cat);
    }
    
}