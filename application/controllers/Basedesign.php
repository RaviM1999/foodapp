<?php

class Basedesign extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->model('Login_model');
        $this->load->library('session');
        $this->load->library('form_validation');
    }
    public function index() {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('Password', 'password', 'required');
        // print_r ($this->form_validation->run());
        // exit;
        if ($this->form_validation->run() == FALSE) {
                    $this->load->view('login');
            } else {
                if($this->input->post('submit')) {
                    $data=array(
                        'email'=>$this->input->post('email'),
                        'password'=>$this->input->post('Password')
                        );
                        // print_r ($data);
                        // exit;
                        $result = $this->Login_model->logindata($data);
                            if($result > 0) {
                                $this->session->set_userdata($data);
                                redirect ('setting');
                            } else {
                                redirect ('login');
                            }
                }
            }    
    }
    public function logout() {
        $this->session->sess_destroy();
        redirect ('login'); 
    }
    public function signup() {
        $this->load->view('signup');
        $this->form_validation->set_rules('fullname', 'Full name', 'required'); 
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('contactnumber', 'Contact Number', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('Password', 'password', 'required');
        if($this->form_validation->run() == TRUE) {
            // echo "success";
            if($this->input->post('submit')) {
                $result=array(
                    'fullname'=>$this->input->post('fullname'),
                    'email'=>$this->input->post('email'),
                    'contactnumber'=>$this->input->post('contactnumber'),
                    'address'=>$this->input->post('address'),
                    'password'=>$this->input->post('Password')
                    );
                    // print_r ($result);
                    // exit;
                   $this->Login_model->signupdata($result);
                   redirect ('Basedesign/index');
            } else {
                // echo 'fail';
            }
        } else {
            // echo "fail";
        }
    }
}