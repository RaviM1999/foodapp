<?php 
class Product extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model('Product_model');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->helper(array('form','url'));
    }
    public function add_product() { 
        if($this->input->post()) {
            foreach ($_FILES as $x => $file) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                if ($x == 'product_image') {
                    $config['file_name']        = date("Ymdhi").$file['name'];
                }                    
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if($x == 'product_image') {
                        if ($this->upload->do_upload('product_image')) {
                            // echo "success---1";
                        } else {
                            echo "Fail";
                        }
                    } 
            }
            $filesname = $_FILES['product_var_image'];
            foreach ($filesname['name'] as $in => $image) {
                $_FILES['product_var_image[]']['name'] = $filesname['name'][$in];
                $_FILES['product_var_image[]']['type'] = $filesname['type'][$in];
                $_FILES['product_var_image[]']['tmp_name'] = $filesname['tmp_name'][$in];
                $_FILES['product_var_image[]']['size'] = $filesname['size'][$in];

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = date("Ymdhi").$image;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('product_var_image[]')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
                    $imagename[] =  $config['file_name'];
            }
                
            $product = $this->input->post();
            $product['product_image'] = date("Ymdhi").$_FILES['product_image']['name'];
            $product['product_var_image'] =  $imagename;
            unset($product['submit2']);
            $product['product_var_variant']  = json_encode($product['product_var_variant']);
            $product['product_var_price']  = json_encode($product['product_var_price']);
            $product['product_var_description']  = json_encode($product['product_var_description']);
            $product['product_var_status']  = json_encode($product['product_var_status']);
            $product['product_var_image']  = json_encode($product['product_var_image']);

            $this->Product_model->add_products($product);
            $currentproduct['details'] = $product;
            $currentproduct['product_var_variant']  = json_decode($product['product_var_variant']);
            $currentproduct['product_var_image']  = json_decode($product['product_var_image']);
            
            $varproduct = array(
                'product_var_variant' => json_decode($product['product_var_variant']),
                'product_var_price' => json_decode($product['product_var_price']),
                'product_var_description' => json_decode($product['product_var_description']),
                'product_var_status'=> json_decode($product['product_var_status']),
                'product_var_image'  => json_decode($product['product_var_image'])
            ); 
            foreach ( $varproduct as $key => $valueArr) {
                foreach($valueArr as $i=>$value) {
                    $filesByInput[$i][$key] = $value;
                }
            }
            $files2 = $filesByInput;
            foreach ($files2 as $dom) {
                $variantinfo[] = $dom;
            }
            $currentproduct['variantinfo'] = $variantinfo;
            $this->view_product($currentproduct);
        } else {
            $this->load->view('add-merchant-item');
        } 
    }
    public function view_product($currentproduct) {   
        $this->load->view('product-view', $currentproduct);
    }
}
