<?php 
class Subcategory extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Merchant_model');
        $this->load->model('Merchantcategory_model');
        $this->load->model('Sub_category_model');
    }
    public function sub_category() {
        if($this->input->post()) {
            $sub_categorys = $this->input->post();
            unset($sub_categorys['submit']);
            $sub_categorys['sub_category_image'] = date("Ymdhi").$_FILES['sub_category_image']['name'];
            $this->Sub_category_model->add_sub_category($sub_categorys);
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']        = date("Ymdhi").$_FILES['sub_category_image']['name'];                   
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
                if ($this->upload->do_upload('sub_category_image')) {
                    // echo "success---1";
                } else {
                    echo "Fail";
                }
        }
        
        $sub_category_list['sub_category_data'] = $this->Sub_category_model->list_sub_category();

        // <----category data for sub category model
        $merchant_data = $this->Merchant_model->merchant();
        $sub_category_list['merchant_data'] = $merchant_data;
        $sub_category_list['category_data'] = $this->Merchantcategory_model->category_data();
        // <----category data for sub category model

        $this->load->view('sub-category', $sub_category_list);
    }
    public function delete_sub_category() {
       $subcategory = $this->input->server('QUERY_STRING');
       $delete_category = $this->Sub_category_model->sub_category_delete($subcategory);
       if ($delete_category == true) {
        redirect ('sub_category');
        }
    }
    public function edit_sub_category() {
        $edit_sub = $this->input->server('QUERY_STRING');
        $editdata['sub_category'] = $this->Sub_category_model->sub_category_edit($edit_sub);
        
        // <----category data for sub category model
        $editdata['category_data'] = $this->Merchantcategory_model->category_data();
        $this->Sub_category_model->edit_sub_category_branch($edit_sub);
        // <----category data for sub category model

        $this->load->view('edit_sub_category', $editdata);
    }

    public function update_sub_category() {
        $update_sub_category = $this->input->post();
        unset($update_sub_category['submit']);
        if (!empty($_FILES['sub_category_image']['name'])) {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']        = date("Ymdhi").$_FILES['sub_category_image']['name'];                    
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
                if ($this->upload->do_upload('sub_category_image')) {
                    echo "success---1";
                } else {
                    echo "Fail";
                }
                $update_sub_category['sub_category_image'] = date("Ymdhi").$_FILES['sub_category_image']['name']; 
        } 
        $this->Sub_category_model->sub_category_update($update_sub_category);
        redirect ('sub_category');
    }
}