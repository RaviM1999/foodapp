<?php 
class Customer extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper("url");
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Customer_model');
    }
    public function customer() {
        if($this->input->post()) {
            $customer = $this->input->post();
           $customer['customer_image'] = date("Ymdhi").$_FILES['customer_image']['name'];
           $this->Customer_model->customer($customer);
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']        = date("Ymdhi").$_FILES['customer_image']['name'];                   
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('customer_image')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
        }
        $customer_list['customer_data'] = $this->Customer_model->customer_list();
        $this->load->view('customer', $customer_list);
    }
    public function customer_delete() {
        $customer_id = $this->input->server('QUERY_STRING');
        $data = $this->Customer_model->customer_delete($customer_id);
        if($data == TRUE) {
            redirect ('customer'); 
        }
    }
    public function customer_edit() {
        $customer_id_edit = $this->input->server('QUERY_STRING');
        $customer_ed['customer_edit'] = $this->Customer_model->customer_edit($customer_id_edit);
        $this->load->view('edit_customer', $customer_ed);
    }
    public function customer_update() {
        $update_customer = $this->input->post();
        if (!empty($_FILES['customer_image']['name'])) {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['file_name']        = date("Ymdhi").$_FILES['customer_image']['name'];                    
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
                if ($this->upload->do_upload('customer_image')) {
                    echo "success---1";
                } else {
                    echo "Fail";
                }
            $update_customer['customer_image'] = date("Ymdhi").$_FILES['customer_image']['name']; 
        }
        $this->Customer_model->customer_update($update_customer);
        redirect ('customer'); 
        
    }
    
}