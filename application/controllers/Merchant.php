<?php
class Merchant extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->database();
        $this->load->library('upload');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('Merchant_model');
    }
    public function merchant() {
       $merchant_list['merchant_data'] = $this->Merchant_model->merchant();  
    //    echo "<pre>";
    //    print_r($merchant_list);
    //    exit;      
        
        $this->load->view('merchant', $merchant_list);
    }
    public function view_merchant() {
        $view_merchantid = $this->input->server('QUERY_STRING');
        $merchant_result =  $this->Merchant_model->view_merchant($view_merchantid);
        //    echo "<pre>";
        //    print_r($merchant_result);
        //    exit;
        $merchnat_info['branch_name']  =  json_decode($merchant_result[0]['branch_name']); 
        $merchant_value = array(
            'branch_name' => json_decode($merchant_result[0]['branch_name']),
            'branch_type' => json_decode($merchant_result[0]['branch_type']),
            'branch_contact_name' => json_decode($merchant_result[0]['branch_contact_name']),
            'branch_contact_number' => json_decode($merchant_result[0]['branch_contact_number']),
            'branch_email' => json_decode($merchant_result[0]['branch_email']),
            'branch_gst' => json_decode($merchant_result[0]['branch_gst']),
            'branch_country' => json_decode($merchant_result[0]['branch_country']),
            'branch_city' => json_decode($merchant_result[0]['branch_city']),
            'branch_state' => json_decode($merchant_result[0]['branch_state']),
            'branch_contact_address' => json_decode($merchant_result[0]['branch_contact_address']),
            'branch_status' => json_decode($merchant_result[0]['branch_status']),
            'branch_food_type' => json_decode($merchant_result[0]['branch_food_type']),
            'branch_category_type' => json_decode($merchant_result[0]['branch_category_type']),
            'branch_logo' => json_decode($merchant_result[0]['branch_logo'])
        );
        foreach ($merchant_value as $key => $valueArr) {
            if (is_array($valueArr)) { // file input "multiple"
                foreach($valueArr as $i=>$value) {
                    $filesByInput[$i][$key] = $value;
                }
            }
        }
        $files2 = $filesByInput;
       foreach ($files2 as $dom) {
            $branch_details[] = $dom;
       }
        $merchnat_info['merchant_val'] = $merchant_result;
        $merchnat_info['branch_value'] = $branch_details;
        $this->load->view('view-merchant-details', $merchnat_info);
    }
    public function edit_merchant() {
       $edit_id = $this->input->server('QUERY_STRING');
       $merchant_var = $this->Merchant_model->edit_merchant($edit_id);
       
       $merchant_branch = array(
        'branch_name' => json_decode($merchant_var[0]['branch_name']),
        'branch_type' => json_decode($merchant_var[0]['branch_type']),
        'branch_contact_name' => json_decode($merchant_var[0]['branch_contact_name']),
        'branch_contact_number' => json_decode($merchant_var[0]['branch_contact_number']),
        'branch_email' => json_decode($merchant_var[0]['branch_email']),
        'branch_gst' => json_decode($merchant_var[0]['branch_gst']),
        'branch_country' => json_decode($merchant_var[0]['branch_country']),
        'branch_city' => json_decode($merchant_var[0]['branch_city']),
        'branch_state' => json_decode($merchant_var[0]['branch_state']),
        'branch_contact_address' => json_decode($merchant_var[0]['branch_contact_address']),
        'branch_status' => json_decode($merchant_var[0]['branch_status']),
        'branch_food_type' => json_decode($merchant_var[0]['branch_food_type']),
        'branch_category_type' => json_decode($merchant_var[0]['branch_category_type']),
        'branch_logo' => json_decode($merchant_var[0]['branch_logo'])
    );
        // echo "<pre>";
        // print_r($merchant_var);
        // exit;
        foreach ( $merchant_branch as $key => $valueArr) {
            if (is_array($valueArr)) { // file input "multiple"
                foreach($valueArr as $i=>$value) {
                    $filesByInput[$i][$key] = $value;
                }
            }
        }
            $files2 = $filesByInput;
            foreach ($files2 as $dom) {
                $branch_list[] = $dom;
        }
        $merchant_call['merchant_in'] = $merchant_var;
        $merchant_call['merchant_info'] = $branch_list;
        $merchant_call['category_type'] =  $this->Merchant_model->merchant_category();
        
        $this->load->view('edit_merchant', $merchant_call);
    }
    public function update_merchant() {
        
        if (!empty($_FILES['merchant_logo']['name'])) {
           foreach ($_FILES as $x => $file) {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                if ($x == 'merchant_logo') {
                    $config['file_name']        = date("Ymdhi").$file['name'];
                }                    
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if($x == 'merchant_logo') {
                        if ($this->upload->do_upload('merchant_logo')) {
                            // echo "success---1";
                        } else {
                            echo "Fail";
                        }
                    } 
            }
            $merchant_update['merchant_logo']  = date("Ymdhi").$_FILES['merchant_logo']['name'];
        }
        if ($_FILES['branch_logo']['name'] != "") {
            $filesname = $_FILES['branch_logo'];
            foreach ($filesname['name'] as $in => $image) {
                $_FILES['branch_logo[]']['name'] = $filesname['name'][$in];
                $_FILES['branch_logo[]']['type'] = $filesname['type'][$in];
                $_FILES['branch_logo[]']['tmp_name'] = $filesname['tmp_name'][$in];
                $_FILES['branch_logo[]']['size'] = $filesname['size'][$in];

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'gif|jpg|png';
                $config['file_name']            = date("Ymdhi").$image;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                    if ($this->upload->do_upload('branch_logo[]')) {
                        // echo "success---1";
                    } else {
                        echo "Fail";
                    }
                
                $imagename[] =  $config['file_name'];
            }
          
            $merchant_update['branch_logo'] = $imagename;
            $merchant_update['branch_logo'] = json_encode($merchant_update['branch_logo']);
        }
        $merchant_update = $this->input->post();

        unset($merchant_update['submit']);

        $merchant_update['branch_name']  = json_encode($merchant_update['branch_name']);
        $merchant_update['branch_type']  = json_encode($merchant_update['branch_type']);
        $merchant_update['branch_contact_name']  = json_encode($merchant_update['branch_contact_name']);
        $merchant_update['branch_contact_number']  = json_encode($merchant_update['branch_contact_number']);
        $merchant_update['branch_email']  = json_encode($merchant_update['branch_email']);
        $merchant_update['branch_gst']  = json_encode($merchant_update['branch_gst']);
        $merchant_update['branch_country']  = json_encode($merchant_update['branch_country']);
        $merchant_update['branch_city']  = json_encode($merchant_update['branch_city']);
        $merchant_update['branch_state']  = json_encode($merchant_update['branch_state']);
        $merchant_update['branch_contact_address']  = json_encode($merchant_update['branch_contact_address']);
        $merchant_update['branch_status']  = json_encode($merchant_update['branch_status']);
        $merchant_update['branch_food_type']  = json_encode($merchant_update['branch_food_type']);
        $merchant_update['branch_category_type']  = json_encode($merchant_update['branch_category_type']);
        $this->Merchant_model->update_merchant($merchant_update);
        redirect ('merchant');
    }
    public function delete_merchant() {
        $merchant_delete =  $this->input->server('QUERY_STRING');
        $merchant_d = $this->Merchant_model->detele_merchant($merchant_delete);
        if ($merchant_d == TRUE) {
            redirect ('merchant');
        }
    }
}