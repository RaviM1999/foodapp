<?php
class Sub_category_model extends CI_Model {
    public function add_sub_category($sub_categorys) {
        $this->db->insert('sub_category', $sub_categorys);
    }
    public function list_sub_category() {
        $sub_list = $this->db->get('sub_category');
        return $sub_list->result();
    }
    public function sub_category_delete($subcategory) {
        $this->db->where('sub_category_id', $subcategory);
        return $this->db->delete('sub_category');
    }
    public function sub_category_edit($edit_sub) {
        $edit_value = $this->db->get_where('sub_category', $edit_sub);
        return $edit_value->result_array();
    }
    public function sub_category_update($edit_sub_category) {
        $sub_category_id = $edit_sub_category['sub_category_id'];
        $this->db->where('sub_category_id', $sub_category_id);
       return $this->db->update('sub_category', $edit_sub_category);
    }
    // merchant branch for edit sub category
    public function edit_sub_category_branch($edit_sub) {
        // echo "<pre>";
        // print_r($edit_sub);
        // exit;
    }

}