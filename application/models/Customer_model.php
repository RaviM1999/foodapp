<?php
class Customer_model extends CI_Model {
    public function customer($customer) {
        $this->db->insert('customer', $customer);
    }
    public function customer_list() {
        $customer = $this->db->get('customer');
        return $customer->result();
    }
    public function customer_delete($customer_id) {
        $this->db->where('customer_id', $customer_id);
        return $this->db->delete('customer');
    }
    public function customer_edit($customer_id_edit) {
        $edit_value = $this->db->get_where('customer', $customer_id_edit);
        return $edit_value->result_array();
    }
    public function customer_update($update_customer) {
        $customerid = $update_customer['customer_id'];
        $this->db->where('customer_id', $customerid);
       return $this->db->update('customer', $update_customer);
    }
}