<?php
class Merchantitem_model extends CI_Model {
    public function productdata() {
        $pro_data = $this->db->get('product');
        return $pro_data->result();
    }
    public function viewdatas($productid) {
        $dataid = $this->db->get_where('product',$productid);
        return $dataid->result_array();
    }
    public function deletedata($productid_item) {
        $this->db->where('product_id', $productid_item);
        return $this->db->delete('product');
    }
    public function editdata($product_edit) {
        $edit_data = $this->db->get_where('product', $product_edit);
        return $edit_data->result_array();
    }
    public function updatedata($update_data) {
        $productid = $update_data['product_id'];
        $this->db->where('product_id', $productid);
       return $this->db->update('product', $update_data);
    }

}