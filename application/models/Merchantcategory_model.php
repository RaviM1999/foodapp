<?php 
class Merchantcategory_model extends CI_Model {
    public function category($category_data) {
        $this->db->insert('category_merchant', $category_data);
    }
    public function category_data() {
       $category = $this->db->get('category_merchant');
       return $category->result();
    }
    public function delete_category($categoreyid) {
        $this->db->where('category_id', $categoreyid);
        return $this->db->delete('category_merchant');
    }
    public function edit_category($edit_datas) {
        $this->db->select('*');
        $this->db->from('category_merchant');
        $this->db->join('merchant', 'category_merchant.category_name = merchant.merchant_restaurant_name');
        $this->db->where('category_merchant.category_id', $edit_datas);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function update_data($update_list) {
        $categoryid = $update_list['category_id'];
        $this->db->where('category_id', $categoryid);
       return $this->db->update('category_merchant', $update_list);
    }
}