<?php 
class Merchant_model extends CI_Model {
    public function add_merchant($merchant_data) {
        $this->db->insert('merchant', $merchant_data);
    }
    public function merchant() {
       $merchant = $this->db->get('merchant');
       return $merchant->result_array();
    }
    public function view_merchant($view_merchantid) {
        $merchantid = $this->db->get_where('merchant', $view_merchantid);
        return $merchantid->result_array();
    }
    public function merchant_category() {
        $merchant = $this->db->get('cuisine_list');
        return $merchant->result();
    }
    public function edit_merchant($edit_id) {
        $edit_value = $this->db->get_where('merchant', $edit_id);
        return $edit_value->result_array();
    }
    public function update_merchant($merchant_update) {
        $merchantid = $merchant_update['merchant_id'];
        $this->db->where('merchant_id', $merchantid);
       return $this->db->update('merchant', $merchant_update);
    }
    public function detele_merchant($merchant_delete) {
        $this->db->where('merchant_id', $merchant_delete);
        return $this->db->delete('merchant');
    }
    public function get_merchant($id) {
        $edit_value = $this->db->get_where('merchant', array('merchant_id ' =>$id));
        return $edit_value->result_array();
    }
}