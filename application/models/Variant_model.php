<?php 
class Variant_model extends CI_Model {
    public function variant_add($variant_data) {
        $this->db->insert('variant_list', $variant_data);
    }
    public function variant_data() {
       $variant_list =  $this->db->get('variant_list');
        return $variant_list->result();
    }
    public function variant_delete($variant_ids) {
        $this->db->where('variant_id', $variant_ids);
       return $this->db->delete('variant_list');
    }
    public function edit_variant($edit_var_id) {
        $edit_var = $this->db->get_where('variant_list', $edit_var_id);
        return $edit_var->result_array();
    }
    public function update_variant($variant_datas) {
        $variant_id = $variant_datas['variant_id'];
        $this->db->where('variant_id', $variant_id);
        $this->db->update('variant_list', $variant_datas);
    }
}